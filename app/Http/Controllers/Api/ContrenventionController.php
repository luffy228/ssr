<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\ContrenventionPieceRepository;
use App\Repositories\Implementation\ContrenventionRepository;
use App\Repositories\Implementation\PieceRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ContrenventionController extends Controller
{
    //
    use ApiResponser;
    protected $contrenventionRepo;
    protected $agentRepo;
    protected $pieceRepo;
    protected $contrenventionPieceRepo;

    function __construct(App $app)
    {

        $this->contrenventionRepo = new ContrenventionRepository($app);
        $this->pieceRepo = new PieceRepository($app);
        $this->contrenventionPieceRepo = new ContrenventionPieceRepository($app);
        $this->agentRepo = new AgentRepository($app);
    }

    public function add(Request $request)
    {

        $arrondissement = $this->agentRepo->show(auth()->guard('api')->user()->user_id);
        $contrenvention_request = [
            'plaque'=>$request["plaque"],
            'agents_id'=>auth()->guard('api')->user()->user_id,
            'arrondissements_id'=>$arrondissement["arrondissements_id"],
        ];
        $contrenvention = $this->contrenventionRepo->create($contrenvention_request);
        foreach ($request["piece"] as $piece) {
            $piece_id = $this->pieceRepo->findName($piece);
            $pieceContrenvention = [
                'contrenventions_id'=>$contrenvention["id"],
                'pieces_id'=> $piece_id->id,
            ];
            $this->contrenventionPieceRepo->create($pieceContrenvention);
        }
        return $this->successResponse("Contrenvention creer avec success");
    }
}
