<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\MessageRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class MessageController extends Controller
{
    //
    use ApiResponser;
    protected $messageRepo;

    function __construct(App $app)
    {
        $this->messageRepo = new MessageRepository($app);
    }

    public function message()
    {
        $visibility = ["All",auth()->guard('api')->user()->type];
        $message = $this->messageRepo->getAllMessagebyType($visibility);
        return $this->successResponse($message);
    }
}
