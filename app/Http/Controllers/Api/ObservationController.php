<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Repositories\Implementation\ContrenventionRepository;
use App\Repositories\Implementation\EnginTypeRepository;
use App\Repositories\Implementation\MotifRepository;
use App\Repositories\Implementation\ObservationRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ObservationController extends Controller
{
    //
    use ApiResponser;
    protected $observationRepo;
    protected $enginRepo;
    protected $motifRepo;
    protected $contrenventionRepo;
     function __construct(App $app)
    {

        $this->observationRepo = new ObservationRepository($app);
        $this->enginRepo = new EnginTypeRepository($app);
        $this->motifRepo = new MotifRepository($app);
        $this->contrenventionRepo = new ContrenventionRepository($app);
    }

    public function add(Request $request)
    {
        if ($request["plaque"] == null) {
            return  $this->errorResponse("La plaque doit etre renseigner",400);
        }

        if ($request["paysortie"] == null) {
            $pays = null;
        }

        if ($request["paysortie"] != null) {
            $country = Country::where('name',$request["paysortie"])
                            ->orWhere('nicename',$request["paysortie"])
                            ->first();
            if ($country != null) {
                $pays = $country["id"];
            }

            if ($country == null) {
                $pays = null;
            }

        }


        $engin_id = $this->enginRepo->find($request["engin"]);
        $motif_id = $this->motifRepo->find($request["motif"]);


        if ($request["sejour"] != null) {
            $finsejour = date('Y-m-d',strtotime('+'.$request["sejour"].'days'));
        }
        if ($request["sejour"] == null) {
            $finsejour = null;
        }
        if (auth()->guard('api')->user()->type == "Police") {
            $visibility = 2;
        }
        if (auth()->guard('api')->user()->type == "Gendarmerie") {
            $visibility = 1;
        }
        $observationRequest = [
            'plaque'=>$request["plaque"],
            'couleur'=>$request["couleur"],
            'permis'=>$request["permis"],
            'sejour'=>$request["sejour"],
            'countriesIn_id'=>213,
            'countriesOut_id'=>$pays,
            'conducteur_nom'=>$request["nom_conducteur"],
            'conducteur_prenom'=>$request["prenom_conducteur"],
            'engin_types_id'=>$engin_id["id"],
            'motifs_id'=>$motif_id["id"],
            'visibilities_id'=>$visibility,
            'agents_id'=>auth()->guard('api')->user()->user_id,
            'finsejour'=>$finsejour
        ];

        $this->observationRepo->create($observationRequest);
        return $this->successResponse("Observation creer avec succes");
    }

    public function  recherche(Request $request)
    {
        $alertes = $this->observationRepo->findPlaqueAlerte($request["plaque"],auth()->guard('api')->user()->type);
        if (count($alertes) == 0) {
            $observation = $this->observationRepo->findPlaqueObservation($request["plaque"],auth()->guard('api')->user()->type);
            if (count($observation ) != 0) {
                $data['conducteur'] = $observation[count($observation )-1]->conducteur_nom ." ". $observation[count($observation )-1]->conducteur_prenom;
                $data['permis'] = $observation[count($observation )-1]->permis;
                $data['engin'] =$observation[count($observation )-1]->engin ;
                $data['couleur'] =$observation[count($observation )-1]->couleur ;
                $data['statut'] = "Observation";
                $data['countAlerte'] = 0;
                $data['countObservation'] = count($observation);
                $contrenvention = $this->contrenventionRepo->PlaqueContrenvention($request["plaque"]);
                $data['countContrenvention'] = count($contrenvention);

                if (count($contrenvention )== 0) {
                    $data['contrenvention'] = "Non";
                }
                if (count($contrenvention )!= 0) {
                    if ($contrenvention[count($contrenvention )-1]->statut == "Valider") {
                        $data['contrenvention'] = "Oui";
                    }
                    if ($contrenvention[count($contrenvention )-1]->statut == "Retirer") {
                        $data['contrenvention'] = "Non";
                    }
                }
                if ($observation[count($observation )-1]->motif == "Sejour") {
                    $data['motif'] ="sejour";
                    if ($observation[count($observation )-1]->countriesOut_id == null) {
                        $data['pays'] = "Non Renseigner";
                    }
                    if ($observation[count($observation )-1]->countriesOut_id != null) {
                        $country = Country::where('id',$observation[count($observation )-1]->countriesOut_id)->first();
                        $data['pays'] = $country["nicename"];
                    }
                    $data['sejour'] = $observation[count($observation )-1]->sejour;
                    $data['entree'] = $observation[count($observation )-1]->debut;
                    $data['finSejour'] = $observation[count($observation )-1]->finsejour;
                    return $this->successResponse($data);
                }
                if ($observation[count($observation )-1]->motif != "Sejour") {
                    $data['motif'] = $observation[count($observation )-1]->motif;
                    return $this->successResponse($data);
                }
            }
            if (count($observation)  == 0) {
                $contrenvention = $this->contrenventionRepo->PlaqueContrenvention($request["plaque"]);
                $data['countContrenvention'] = count($contrenvention);

                if (count($contrenvention )== 0) {
                    $data['contrenvention'] = "Non";
                }
                if (count($contrenvention )!= 0) {
                    if ($contrenvention[count($contrenvention )-1]->statut == "Valider") {
                        $data['contrenvention'] = "Oui";
                    }
                    if ($contrenvention[count($contrenvention )-1]->statut == "Retirer") {
                        $data['contrenvention'] = "Non";
                    }
                }
                return $this->successResponse($data);
            }
        }
        if (count($alertes) != 0) {
            $data['conducteur'] = $alertes[count($alertes )-1]->conducteur_nom ." ". $alertes[count($alertes )-1]->conducteur_prenom;
            $data['permis'] = $alertes[count($alertes )-1]->permis;
            $data['engin'] =$alertes[count($alertes )-1]->engin ;
            $data['couleur'] =$alertes[count($alertes )-1]->couleur ;
            if ($alertes[count($alertes )-1]->statut == 0) {
                $data['statut'] = "Alerte";
            }
            if ($alertes[count($alertes )-1]->statut == 1) {
                $data['statut'] = "Rien a signaler";
            }
            $data['countAlerte'] =count($alertes);
            $data['countObservation'] = count($alertes);
            $contrenvention = $this->contrenventionRepo->PlaqueContrenvention($request["plaque"]);
            $data['countContrenvention'] = count($contrenvention);

            if (count($contrenvention )== 0) {
                    $data['contrenvention'] = "Non";
            }
            if (count($contrenvention )!= 0) {
                    if ($contrenvention[count($contrenvention )-1]->statut == "Valider") {
                        $data['contrenvention'] = "Oui";
                    }
                    if ($contrenvention[count($contrenvention )-1]->statut == "Retirer") {
                        $data['contrenvention'] = "Non";
                    }
            }
            if ($alertes[count($alertes )-1]->motif == "Sejour") {
                $data['motif'] ="sejour expirer";
                if ($alertes[count($alertes )-1]->countriesOut_id == null) {
                    $data['pays'] = "Non Renseigner";
                }
                if ($alertes[count($alertes )-1]->countriesOut_id != null) {
                    $country = Country::where('id',$alertes[count($alertes )-1]->countriesOut_id)->first();
                    $data['pays'] = $country["nicename"];
                }
                $data['sejour'] = $alertes[count($alertes )-1]->sejour;
                $data['entree'] = $alertes[count($alertes )-1]->debut;
                $data['finSejour'] = $alertes[count($alertes )-1]->finsejour;
                return $this->successResponse($data);
            }
            if ($alertes[count($alertes )-1]->motif != "Sejour") {
                $data['motif'] = $alertes[count($alertes )-1]->motif;
                return $this->successResponse($data);
            }

        }
    }
}
