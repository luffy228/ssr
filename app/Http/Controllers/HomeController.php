<?php

namespace App\Http\Controllers;

use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    protected $userRepo;
    protected $agentRepo;
    function __construct(App $app)
    {
        $this->userRepo = new UserRepository($app);
        $this->agentRepo = new AgentRepository($app);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index()
    {
        return view('welcome');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function adminPage(Request $request)
    {
        if(Auth::attempt(['username' => request('name'), 'password' => request('password')])){

            $role = Auth::user()->getRoleNames()[0];
            if ($role == "SuperAdministrateur" ||$role == "Administrateur" ||$role == "SousAdministrateur") {
                return redirect()->route('home');
            }

            return redirect()->back();
        }
        else{
            return redirect()->back();
        }

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
