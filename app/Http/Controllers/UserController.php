<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\ArrondissementRepository;
use App\Repositories\Implementation\UserRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    //
    protected $userRepo;
    protected $agentRepo;
    protected $arrondissementRepo;
    use ApiResponser;
     function __construct(App $app)
    {
        $this->userRepo = new UserRepository($app);
        $this->agentRepo = new AgentRepository($app);
        $this->arrondissementRepo = new ArrondissementRepository($app);
    }

    public function logout()
    {
        $token = Auth::guard('api')->user()->token();
        $token->revoke();
        return $this->successResponse(null, 'You have been successfully logged out!', 201);
    }

    public function login()
    {
        if(Auth::attempt(['telephone' => request('name'), 'password' => request('password')])){
            $user = Auth::user();
            $role = $user->getRoleNames();

            if ($role[0] == "Controleur") {
                $data['token'] =  $user->createToken('token')->accessToken;
                $data['role'] = "Controleur";
                $data['information'] = $this->userRepo->informationControlleur($user->id);
                return $this->successResponse($data);
            }

            if ($role[0] != "Controleur") {
                return $this->errorResponse('Vous n avez pas le droit necessaire', 403);
            }
        }
        else{
            return $this->errorResponse('Authentification failled: email or password incorrect', 403);
        }
    }

}

