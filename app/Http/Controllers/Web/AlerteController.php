<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AlerteRepository;
use App\Repositories\Implementation\ObservationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AlerteController extends Controller
{
    //
    protected $alerteRepo;
    protected $observationRepo;
     function __construct(App $app)
    {
        $this->middleware('auth');
        $this->alerteRepo = new AlerteRepository($app);
        $this->observationRepo = new ObservationRepository($app);

    }

    public function listeAlerte($type)
    {
        if ($type == "police") {

            $visibility = ['2','3'] ;
            $observation = $this->getliste($visibility);
            return view('pages.police.listAlertPolice',compact('observation'));
        }

        if ($type == "gendarmerie") {
            $visibility = ['1','3'] ;
            $observation = $this->getliste($visibility);
            return view('pages.gendarmerie.listAlertGendarmerie',compact('observation'));

        }

    }

    public function listeAlerteResolue($type)
    {
        if ($type == "police") {

            $visibility = ['2','3'] ;
            $observation = $this->getlisteResolue($visibility);
            return view('pages.police.listAlertResoluePolice',compact('observation'));
        }

        if ($type == "gendarmerie") {
            $visibility = ['1','3'] ;
            $observation = $this->getlisteResolue($visibility);
            return view('pages.gendarmerie.listAlertResolueGendarmerie',compact('observation'));
        }

    }

    public function getlisteResolue($visibility)
    {
        $admin = $this->alerteRepo->listResolue($visibility);
        return $admin;
    }

    public function getliste($visibility)
    {
        $admin = $this->alerteRepo->list($visibility);
        return $admin;
    }

    public function changeVisibility(Request $request)
    {
        $observationRequest =
        [
            'visibilities_id'=>3
        ];
        $this->observationRepo->update($observationRequest,$request["observation"]);
        if ($request["type"] == "Police") {
            return redirect()->route('listeAlerte',["type"=>"police"]);
        }
        if ($request["type"] == "Gendarmerie") {
            return redirect()->route('listeAlerte',["type"=>"gendarmerie"]);
        }
    }

    public function alerteResolue(Request $request)
    {
        $alerteRequest =
        [
            'statut'=>1,
            'agents_id'=>Auth::user()->user_id
        ];
        $this->alerteRepo->update($alerteRequest,$request["observation"]);
        if (Auth::user()->type == "Police") {
            return redirect()->route('listeAlerte',["type"=>"police"]);
        }
        if (Auth::user()->type == "Gendarmerie") {
            return redirect()->route('listeAlerte',["type"=>"gendarmerie"]);
        }
    }

    public function add(Request $request)
    {
        $alerteRequest = [
            'statut'=>0,
            'observations_id'=>$request["observation"],
            'agents_id'=>Auth::user()->user_id
        ];
        $this->alerteRepo->create($alerteRequest);
        if ($request["type"] == "Police") {
            return redirect()->route('listeAlerte',["type"=>"police"]);
        }

        if ($request["type"] == "Gendarmerie") {
            return redirect()->route('listeAlerte',["type"=>"gendarmerie"]);
        }
    }
}
