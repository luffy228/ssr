<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\ArrondissementRepository;
use App\Repositories\Implementation\ForceTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ArrondissementController extends Controller
{
    //
    protected $arrondissementRepo;
    protected $forceTypeRepo;

    function __construct(App $app)
    {
        $this->middleware('auth');
        $this->arrondissementRepo = new ArrondissementRepository($app);
        $this->forceTypeRepo = new ForceTypeRepository($app);
    }

    public function add(Request $request)
    {
        $force = $this->forceTypeRepo->findName($request["force"]);
        $this->arrondissementRepo->validateData();
        $form_request = [
            'numero'=>$request["numero"],
            'telephone'=>$request["telephone"],
            'force_types_id'=>$force["id"]
        ];
        $this->arrondissementRepo->create($form_request);

        if ($request["force"] == "Police") {
            return redirect()->route('listearrondissement',["type"=>"police"]);
        }

        if ($request["force"] == "Gendarmerie") {
            return redirect()->route('listearrondissement',["type"=>"gendarmerie"]);
        }
    }

    public function listArrondissement($type)
    {

        if ($type == "police") {
            $type = "2";
            $police = $this->liste($type);
            return view('pages.police.ListArrondissementPolice',compact('police'));
        }

        if ($type == "gendarmerie") {
            $type = "1";
            $gendarme = $this->liste($type);
            return view('pages.gendarmerie.ListArrondissementGendarmerie',compact('gendarme'));
        }

    }

    public function liste($type)
    {
      $force = $this->arrondissementRepo->liste($type);
      return $force;
    }
}
