<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class GeneriqueController extends Controller
{
    //
    protected $userRepo;

    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->userRepo = new UserRepository($app);
    }

    public function index()
    {
        return view('home');
    }

    public function profile()
    {
        return view('profile');
    }

    public function updateCompte(Request $request)
    {
        $data = [];
        if ($request["name"] != null) {
            $data["name"] = $request["name"];
        }
        if ($request["firstname"] != null) {
            $data["firstname"] = $request["firstname"];
        }
        if ($request["phone"] != null) {
            $data["telephone"] = $request["phone"];
        }
        if ($request["username"] != null) {
            $data["username"] = $request["username"];
        }
        if ($request["email"] != null) {
            $data["email"] = $request["email"];
        }

        $this->userRepo->update($data,Auth::user()->id);
        return redirect()->back();
    }

    public function updatePassword(Request $request)
    {

        $data = [];
        if ($request["lastpassword"] == null) {
            return redirect()->back();
        }


        if (Hash::check($request["lastpassword"], Auth::user()->password)) {
            $data["password"] = bcrypt($request["newpassword"]);
            $this->userRepo->update($data,Auth::user()->id);
            return redirect()->back();
        }
        return redirect()->back();


    }


}
