<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MapsController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth');
    }

    public function maps($type)
    {
         if ($type == "police") {

            return view('pages.police.MapsPolice');
        }

        if ($type == "gendarmerie") {

            return view('pages.gendarmerie.MapsGendarmerie');
        }


    }
}
