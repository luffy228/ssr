<?php

namespace App\Http\Controllers\web;

use Ably\AblyRest;
use App\Http\Controllers\Controller;
use App\Repositories\Implementation\MessageRepository;
use App\Traits\ExternalApi;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class MesssagerieController extends Controller
{
    //
    protected $messageRepo;
    use ExternalApi;
    function __construct(App $app)
    {
        $this->middleware('auth');
        $this->messageRepo = new MessageRepository($app);
    }

    public function messageAll()
    {
        if (Auth::user()->type == "Admin") {
            $message = $this->messageRepo->getLastMessagebyUser(Auth::user()->id);
            $adminMessage=[];
        }

        if (Auth::user()->type == "Police") {
            $message = $this->messageRepo->getLastMessagebyUser(Auth::user()->id);
            $adminMessage = $this->messageRepo->getLastMessagebyAdmin();

        }

        if (Auth::user()->type == "Gendarmerie") {
            $message = $this->messageRepo->getLastMessagebyUser(Auth::user()->id);
            $adminMessage = $this->messageRepo->getLastMessagebyAdmin();
        }
        return view('pages.police.MessagePolice',compact("message","adminMessage"));
    }

    public function messageHistory()
    {
            $message = $this->messageRepo->getAllMessagebyUser(Auth::user()->id);
            return view('conversation',compact("message"));
    }

    public function messageHistoryAdmin()
    {

            $message = $this->messageRepo->getAllMessagebyAdmin();

        // par rapport au type d'utilisateur envoyer les informations qu'il faut
            return view('conversationAdmin',compact("message"));
    }

    public function messageSend(Request $request)
    {


        if ($request["message"] == null) {
            return redirect()->back();
        }

        // la visibiliter depend du type de l'utilisateur

        if (Auth::user()->type == "Admin") {
            $visibility = "All";
        }
        if (Auth::user()->type == "Police") {
            $visibility = "Police";
        }
        if (Auth::user()->type == "Gendarmerie") {
            $visibility = "Gendarmerie";
        }

        $message_request = [
            'objet'=>$request["objet"],
            'message'=>$request["message"],
            'senders_id'=>Auth::user()->id,
            'visibility'=>$visibility,
        ];
        try {
            $ably = new AblyRest($this->ablyToken());
            $ably->time();
            $ably->channel($this->ablyChannel())->publish('',$message_request);

        } catch (Exception $e) {
                return redirect()->back();
        }

        $this->messageRepo->create($message_request);
        return redirect()->route('listemessage');
    }
}
