<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\EnginTypeRepository;
use App\Repositories\Implementation\MotifRepository;
use App\Repositories\Implementation\ObservationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ObservationController extends Controller
{
    //
    protected $observationRepo;
    protected $enginRepo;
    protected $motifRepo;
    function __construct(App $app)
    {
        $this->middleware('auth');
        $this->observationRepo = new ObservationRepository($app);
        $this->enginRepo = new EnginTypeRepository($app);
        $this->motifRepo = new MotifRepository($app);


    }

    public function ObservationListe($type)
    {
        $namearrondissement =  DB::table('users')
                                  ->where('users.user_id',Auth::user()->user_id)
                                  ->join('agents','users.user_id','=','agents.id')
                                  ->join('arrondissements','agents.arrondissements_id','=','arrondissements.id')
                                  ->select('arrondissements.numero as arrondissement','arrondissements.id as arrondissement_id')
                                  ->first();
        if ($type == "police") {

            $visibility = ['2','3'] ;
            $alertes = DB::table('alertes')->select('alertes.observations_id')->get();
            $alertesObservation = [];
            foreach ($alertes as $alertes) {
                array_push($alertesObservation,$alertes->observations_id);
            }
            $observation = $this->getObservation($visibility,$namearrondissement->arrondissement_id,$alertesObservation);
            return view('pages.police.ListObservationPolice',compact('observation'));
        }

        if ($type == "gendarmerie") {
            $visibility = ['1','3'] ;
            $alertes = DB::table('alertes')->select('alertes.observations_id')->get();
            $alertesObservation = [];
            foreach ($alertes as $alertes) {
                array_push($alertesObservation,$alertes->observations_id);
            }

            $observation = $this->getObservation($visibility,$namearrondissement->arrondissement_id,$alertesObservation);
            return view('pages.gendarmerie.ListObservationGendarmerie',compact('observation'));
        }

    }

    public function  ObservationFrontiereListe()
    {
        $visibility = ['2','3'] ;
        $alertes = DB::table('alertes')->select('alertes.observations_id')->get();
        $alertesObservation = [];
        foreach ($alertes as $alertes) {
            array_push($alertesObservation,$alertes->observations_id);
        }
        $observation = $this->getObservationFrontiere($visibility,$alertesObservation);
        return view('pages.police.ListObservationFrontierePolice',compact('observation'));
    }

    public function getObservation($visibility,$idarrondissement,$alertesObservation)
    {
        $admin = $this->observationRepo->listobservation($visibility,$idarrondissement,$alertesObservation);
        return $admin;
    }

    public function getObservationFrontiere($visibility,$alertesObservation)
    {
        $admin = $this->observationRepo->listobservationfrontiere($visibility,$alertesObservation);
        return $admin;
    }
}
