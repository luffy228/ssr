<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Repositories\Implementation\ContrenventionRepository;
use App\Repositories\Implementation\ObservationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class RechercheController extends Controller
{

    protected $observationRepo;
    protected $contrenventionRepo;

    function __construct(App $app)
    {
        $this->middleware('auth');
        $this->observationRepo = new ObservationRepository($app);
        $this->contrenventionRepo = new ContrenventionRepository($app);

    }

    public function recherche(Request $request)
    {
        $alertes = $this->observationRepo->findPlaqueAlerte($request["plaque"],Auth::user()->type);
        $data["plaque"] = $request["plaque"];
        if (count($alertes) == 0) {
            $observation = $this->observationRepo->findPlaqueObservation($request["plaque"],Auth::user()->type);
            if (count($observation ) != 0) {

                $data['conducteur'] = $observation[count($observation )-1]->conducteur_nom ." ". $observation[count($observation )-1]->conducteur_prenom;
                $data['permis'] = $observation[count($observation )-1]->permis;
                $data['engin'] =$observation[count($observation )-1]->engin ;
                $data['couleur'] =$observation[count($observation )-1]->couleur ;
                $data['statut'] = "Observation";
                $data['countAlerte'] = 0;
                $data['countObservation'] = count($observation);
                $contrenvention = $this->contrenventionRepo->PlaqueContrenvention($request["plaque"]);
                $data['countContrenvention'] = count($contrenvention);

                if (count($contrenvention )== 0) {
                    $data['contrenvention'] = "Non";
                }
                if (count($contrenvention )!= 0) {
                    if ($contrenvention[count($contrenvention )-1]->statut == "Valider") {
                        $data['contrenvention'] = "Oui";
                    }
                    if ($contrenvention[count($contrenvention )-1]->statut == "Retirer") {
                        $data['contrenvention'] = "Non";
                    }
                }
                if ($observation[count($observation )-1]->motif == "Sejour") {
                    $data['motif'] ="sejour";
                    if ($observation[count($observation )-1]->countriesOut_id == null) {
                        $data['pays'] = "Non Renseigner";
                    }
                    if ($observation[count($observation )-1]->countriesOut_id != null) {
                        $country = Country::where('id',$observation[count($observation )-1]->countriesOut_id)->first();
                        $data['pays'] = $country["nicename"];
                    }
                    $data['sejour'] = $observation[count($observation )-1]->sejour;
                    $data['entree'] = $observation[count($observation )-1]->debut;
                    $data['finSejour'] = $observation[count($observation )-1]->finsejour;

                    return view('recherche',compact('data','observation','alertes','contrenvention'));
                }
                if ($observation[count($observation )-1]->motif != "Sejour") {
                    $data['motif'] = $observation[count($observation )-1]->motif;
                    return view('recherche',compact('data','observation','alertes','contrenvention'));
                }
            }
            if (count($observation)  == 0) {
                $contrenvention = $this->contrenventionRepo->PlaqueContrenvention($request["plaque"]);
                $data['countContrenvention'] = count($contrenvention);

                if (count($contrenvention )== 0) {
                    $data['contrenvention'] = "Non";
                }
                if (count($contrenvention )!= 0) {
                    if ($contrenvention[count($contrenvention )-1]->statut == "Valider") {
                        $data['contrenvention'] = "Oui";
                    }
                    if ($contrenvention[count($contrenvention )-1]->statut == "Retirer") {
                        $data['contrenvention'] = "Non";
                    }
                }
                $data['statut'] = null;
                return view('recherche',compact('data','observation','alertes','contrenvention'));
            }
        }
        if (count($alertes) != 0) {
            $data['conducteur'] = $alertes[count($alertes )-1]->conducteur_nom ." ". $alertes[count($alertes )-1]->conducteur_prenom;
            $data['permis'] = $alertes[count($alertes )-1]->permis;
            $data['engin'] =$alertes[count($alertes )-1]->engin ;
            $data['couleur'] =$alertes[count($alertes )-1]->couleur ;
            if ($alertes[count($alertes )-1]->statut == 0) {
                $data['statut'] = "Alerte";
            }
            if ($alertes[count($alertes )-1]->statut == 1) {
                $data['statut'] = "Rien a signaler";
            }
            $data['countAlerte'] =count($alertes);
            $data['countObservation'] = count($alertes);
            $contrenvention = $this->contrenventionRepo->PlaqueContrenvention($request["plaque"]);
            $data['countContrenvention'] = count($contrenvention);

            if (count($contrenvention )== 0) {
                    $data['contrenvention'] = "Non";
            }
            if (count($contrenvention )!= 0) {
                    if ($contrenvention[count($contrenvention )-1]->statut == "Valider") {
                        $data['contrenvention'] = "Oui";
                    }
                    if ($contrenvention[count($contrenvention )-1]->statut == "Retirer") {
                        $data['contrenvention'] = "Non";
                    }
            }
            if ($alertes[count($alertes )-1]->motif == "Sejour") {
                $data['motif'] ="sejour expirer";
                if ($alertes[count($alertes )-1]->countriesOut_id == null) {
                    $data['pays'] = "Non Renseigner";
                }
                if ($alertes[count($alertes )-1]->countriesOut_id != null) {
                    $country = Country::where('id',$alertes[count($alertes )-1]->countriesOut_id)->first();
                    $data['pays'] = $country["nicename"];
                }
                $data['sejour'] = $alertes[count($alertes )-1]->sejour;
                $data['entree'] = $alertes[count($alertes )-1]->debut;
                $data['finSejour'] = $alertes[count($alertes )-1]->finsejour;
                return view('recherche',compact('data','alertes','contrenvention'));
            }
            if ($alertes[count($alertes )-1]->motif != "Sejour") {
                $data['motif'] = $alertes[count($alertes )-1]->motif;
                return view('recherche',compact('data','alertes','contrenvention'));
            }

        }

    }
}
