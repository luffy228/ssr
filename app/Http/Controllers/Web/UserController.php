<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\ArrondissementRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    //
    protected $userRepo;
    protected $agentRepo;
    protected $arrondissementRepo;
    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->userRepo = new UserRepository($app);
        $this->agentRepo = new AgentRepository($app);
        $this->arrondissementRepo = new ArrondissementRepository($app);
    }

    public function listAdmin($type)
    {
        if ($type == "police") {
            $name = "Police"; // revoir cette fonction
            $role = "2";
            $police = $this->liste($name,$role);
            return view('pages.police.ListAdminPolice',compact('police'));
        }

        if ($type == "gendarmerie") {
            $name = "Gendarmerie"; // revoir cette fonction aussi
            $role = "2";
            $gendarme = $this->liste($name,$role);
            return view('pages.gendarmerie.ListAdminGendarmerie',compact('gendarme'));
        }
    }

    public function addAdministrateur(Request $request)
    {
            $valider =  Validator::make(request()->all(),[
                "telephone" => 'regex:/^[0-9]{8}$/'
            ]);
            if($valider->fails()) {
                return redirect()->back();
            }


            if ($request["type"] == null && $request["type"]!= 'Police' && $request["type"]!= 'Gendarmerie')
            {
                return redirect()->back();
            }else {
                $form_request = [
                    'name'=>$request["name"],
                    'firstname'=>$request["firstname"],
                    'username'=>$request["username"],
                    'password'=>bcrypt($request["password"]),
                    'email'=>$request["email"],
                    'telephone'=>$request["telephone"],
                    'type'=>$request["type"]
                ];
                $user = $this->userRepo->create($form_request);
                $role1 = Role::find(2);
                $user->assignRole($role1);
                if ($request["type"]== 'Police') {
                    return redirect()->route('listeadmin',["type"=>"police"]);
                }
                if ($request["type"]== 'Gendarmerie') {
                    return redirect()->route('listeadmin',["type"=>"gendarmerie"]);
                }

            }
    }

    public function SousAdminList($type)
    {

        if ($type == "police") {
            $name = "Police"; // revoir cette fonction
            $role = "3";
            $police = $this->listsousAdmin($name,$role);
            $type = "2";
            $arrondissement = $this->arrondissementRepo->liste($type);
            return view('pages.police.ListSousAdminPolice',compact('police','arrondissement'));
        }

        if ($type == "gendarmerie") {
            $name = "Gendarmerie"; // revoir cette fonction aussi
            $role = "3";
            $gendarme = $this->listsousAdmin($name,$role);
            $type = "1";
            $arrondissement = $this->arrondissementRepo->liste($type);
            return view('pages.gendarmerie.ListSousAdminGendarmerie',compact('gendarme','arrondissement'));
        }
    }

    public function addSousCompte(Request $request)
    {
        $valider =  Validator::make(request()->all(),[
            "telephone" => 'regex:/^[0-9]{8}$/'
        ]);
        if($valider->fails()) {
            return redirect()->back();
        }
            if ($request["type"] == null && $request["type"]!= 'Admin' && $request["type"]!= 'Police' && $request["type"]!= 'Gendarmerie')
            {
                return redirect()->back();
            }else {
                $form_request = [
                    'name'=>$request["name"],
                    'firstname'=>$request["firstname"],
                    'username'=>$request["username"],
                    'password'=>bcrypt($request["password"]),
                    'email'=>$request["email"],
                    'telephone'=>$request["telephone"],
                    'type'=>$request["type"]
                ];
                $user = $this->userRepo->create($form_request);
                $role1 = Role::find(3);
                $user->assignRole($role1);
                if ($request["type"]== 'Police') {
                    return redirect()->route('listesousadmin',["type"=>"police"]);
                }
                if ($request["type"]== 'Gendarmerie') {
                    return redirect()->route('listesousadmin',["type"=>"gendarmerie"]);
                }
            }
    }

    public function affecterSousAdmin(Request $request)
    {

        $arrondissement = $this->arrondissementRepo->findNumero($request["arrondissement"]);
        $agentRequest = [
            'arrondissements_id'=>$arrondissement["id"]
        ];
        if ($arrondissement == null) {
            return redirect()->back();
        }

        $agent = $this->agentRepo->create($agentRequest);
        $user = User::find($request['user']);


        $userRequest =
        [
            'user_type'=>$this->agentRepo->model(),
            'user_id'=>$agent->id
        ];
        $this->userRepo->update($userRequest,$user['id']);

        if ($user->type== 'Police') {
            return redirect()->route('listesousadmin',["type"=>"police"]);
        }
        if ($user->type== 'Gendarmerie') {
            return redirect()->route('listesousadmin',["type"=>"gendarmerie"]);
        }

    }

    public function listecontrolleur($type)
    {

        if ($type == "police") {
            $name = "Police"; // revoir cette fonction
            $role = "4";
            $police = $this->listcontrolleur($name,$role);
            return view('pages.police.ListControleurPolice',compact('police'));
        }

        if ($type == "gendarmerie") {
            $name = "Gendarmerie"; // revoir cette fonction aussi
            $role = "4";
            $gendarme = $this->listcontrolleur($name,$role);
            return view('pages.gendarmerie.ListControleurGendarmerie',compact('gendarme'));
        }


    }

    public function listcontrolleur($name,$role)
    {
        $admin = $this->userRepo->listcontrolleur($name,$role);
        return $admin;
    }



    public function addControlleur(Request $request)
    {
        $valider =  Validator::make(request()->all(),[
            "telephone" => 'regex:/^[0-9]{8}$/'
        ]);
        if($valider->fails()) {
            return redirect()->back();
        }
        if ($request["type"] == null && $request["type"]!= 'Admin' && $request["type"]!= 'Police' && $request["type"]!= 'Gendarmerie')
        {
            return redirect()->back();
        }else {

            $arrondissement = DB::table('users')
                                ->where('users.user_id',Auth::user()->user_id)
                                ->join('agents','users.user_id','=','agents.id')
                                ->join('arrondissements','agents.arrondissements_id','=','arrondissements.id')
                                ->select('arrondissements.numero as arrondissement','arrondissements.id as arrondissement_id')
                                ->first();
            

            $form_request = [
                'name'=>$request["name"],
                'firstname'=>$request["firstname"],
                'username'=>$request["username"],
                'password'=>bcrypt($request["password"]),
                'email'=>$request["email"],
                'telephone'=>$request["telephone"],
                'type'=>$request["type"]
            ];
            $user = $this->userRepo->create($form_request);
            $role1 = Role::find(4);
            $user->assignRole($role1);
            $agentRequest = [
                'arrondissements_id'=>$arrondissement->arrondissement_id
            ];
            $agent = $this->agentRepo->create($agentRequest);
            $userRequest =
            [
                'user_type'=>$this->agentRepo->model(),
                'user_id'=>$agent->id
            ];
            $this->userRepo->update($userRequest,$user->id);
            if ($request["type"]== 'Police') {
                return redirect()->route('listecontroleur',["type"=>"police"]);
            }
            if ($request["type"]== 'Gendarmerie') {
                return redirect()->route('listecontroleur',["type"=>"gendarmerie"]);
            }
        }

    }



    public function liste($name, $role)
    {
      $admin = $this->userRepo->listeAdmin($name,$role);
      return $admin;
    }

    public function listsousAdmin($name,$role)
    {
        $admin = $this->userRepo->listesousAdmin($name,$role);
        return $admin;
    }


}
