<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    //
    protected $fillable = ['longitude','latitude','grades_id','arrondissements_id'];
    public function userMother()
    {
        return $this->morphOne('App\Models\User','userType');
    }

    public function nameModel()
    {
        return 'Agent';
    }

    public function observations()
    {
        return $this->hasMany('App\Models\Observation');
    }

    public function alertes()
    {
        return $this->hasMany('App\Models\Alerte');
    }

    public function grades()
    {
        return $this->belongsTo('App\Models\Grade');
    }

    public function arrondissements()
    {
        return $this->belongsTo('App\Models\Arrondissement');
    }





}
