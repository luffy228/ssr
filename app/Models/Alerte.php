<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alerte extends Model
{
    //
    protected $fillable = ['statut','agents_id','observations_id'];
    public function nameModel()
    {
        return 'Alerte';
    }


    public function visibilities()
    {
        return $this->belongsTo('App\Models\Visibility');
    }

    public function agents()
    {
        return $this->belongsTo('App\Models\Agent');
    }

}
