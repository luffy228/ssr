<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Arrondissement extends Model
{
    //
    protected $fillable = ['telephone','numero','force_types_id'];
    public function nameModel()
    {
        return 'Arrondissement';
    }

    public function agents()
    {
        return $this->hasMany('App\Models\Agent');
    }

    public function forcesTypes()
    {
        return $this->belongsTo('App\Models\ForceType');
    }



}
