<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //


    public function nameModel()
    {
        return 'Country';
    }

    public function observations()
    {
        return $this->hasMany('App\Models\Observation');
    }

}
