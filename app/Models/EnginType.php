<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnginType extends Model
{
    //
    public function nameModel()
    {
        return 'EnginType';
    }

    public function observations()
    {
        return $this->hasMany('App\Models\Observation');
    }

}
