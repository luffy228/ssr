<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForceType extends Model
{
    //
    public function nameModel()
    {
        return 'ForceType';
    }

    public function arrondissements()
    {
        return $this->hasMany('App\Models\Arrondissement');
    }

    public function grades()
    {
        return $this->hasMany('App\Models\Grade');
    }

}
