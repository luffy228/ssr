<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    //
    public function nameModel()
    {
        return 'Grade';
    }

    public function agents()
    {
        return $this->hasMany('App\Models\Agent');
    }

    public function forcesTypes()
    {
        return $this->belongsTo('App\Models\ForceType');
    }

}
