<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $fillable = ['objet','message','senders_id','read','visibility'];
    public function nameModel()
    {
        return 'Message';
    }
}
