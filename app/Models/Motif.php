<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Motif extends Model
{
    //
    public function nameModel()
    {
        return 'Motif';
    }

    public function observations()
    {
        return $this->hasMany('App\Models\Observation');
    }

}
