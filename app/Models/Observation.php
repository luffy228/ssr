<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Observation extends Model
{
    //
    protected $fillable = ['plaque','couleur','permis','engin_types_id','motifs_id','countriesIn_id','countriesOut_id','visibilities_id','agents_id','sejour','conducteur_nom','conducteur_prenom','finsejour'];
    public function nameModel()
    {
        return 'Observation';
    }



    public function engins()
    {
        return $this->belongsTo('App\Models\EnginType');
    }

    public function motifs()
    {
        return $this->belongsTo('App\Models\Motif');
    }

    public function countries()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function visibilities()
    {
        return $this->belongsTo('App\Models\Visibility');
    }

    public function agents()
    {
        return $this->belongsTo('App\Models\Agent');
    }

}
