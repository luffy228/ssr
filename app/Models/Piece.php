<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    //
    protected $fillable = ['description'];
    public function nameModel()
    {
        return 'Piece';

    }
}
