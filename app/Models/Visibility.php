<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visibility extends Model
{
    //
    public function nameModel()
    {
        return 'Visibility';
    }

    public function observations()
    {
        return $this->hasMany('App\Models\Observation');
    }
    public function alertes()
    {
        return $this->hasMany('App\Models\Alerte');
    }

}
