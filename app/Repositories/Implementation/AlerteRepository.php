<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AlerteRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\Alerte';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function list($visibility)
    {
        $record = DB::table('alertes')
                        ->join('observations','alertes.observations_id','=','observations.id')
                        ->whereIn('visibilities_id',$visibility)
                        ->where('alertes.statut',0)
                        ->join('engin_types','observations.engin_types_id','=','engin_types.id')
                        ->join('motifs','observations.motifs_id','=','motifs.id')
                        ->join('agents','observations.agents_id','=','agents.id')
                        ->select('alertes.id','observations.id  as observation_id','observations.visibilities_id','observations.countriesOut_id','observations.plaque','observations.couleur','observations.permis','observations.conducteur_nom','observations.conducteur_prenom','engin_types.description as engin','motifs.description as motif')
                        ->get();
        return $record;
    }

    public function listResolue($visibility)
    {
        $record = DB::table('alertes')
                        ->join('observations','alertes.observations_id','=','observations.id')
                        ->whereIn('visibilities_id',$visibility)
                        ->where('alertes.statut',1)
                        ->join('engin_types','observations.engin_types_id','=','engin_types.id')
                        ->join('motifs','observations.motifs_id','=','motifs.id')
                        ->join('agents','observations.agents_id','=','agents.id')
                        ->join('users','agents.id','=','users.user_id')
                        ->join('arrondissements','agents.arrondissements_id','=','arrondissements.id')
                        ->select('alertes.id','alertes.updated_at','observations.id  as observation_id','observations.visibilities_id','observations.countriesOut_id','observations.plaque','observations.couleur','observations.permis','observations.conducteur_nom','observations.conducteur_prenom','engin_types.description as engin','motifs.description as motif','users.name','users.firstname','arrondissements.numero')
                        ->get();
        return $record;

    }




}
