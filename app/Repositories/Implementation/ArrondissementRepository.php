<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\Validator;

class ArrondissementRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\Arrondissement';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function liste($type)
    {
        return $this->model
                    ->where('force_types_id',$type)
                    ->select('telephone','numero')
                    ->get();
    }

    public function findNumero($numero)
    {
        return $this->model
                    ->where('numero',$numero)
                    ->first();
    }



}
