<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MessageRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\Message';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function getLastMessagebyUser($user_id)
    {
        $record = $this->model->where('senders_id',$user_id)->latest()->first();
        return $record;

    }

    public function getLastMessagebyAdmin()
    {
        $record = DB::table('messages')->where('visibility',"All")->latest()->first();
        return $record;

    }

    public function getAllMessagebyUser($user_id)
    {
        $record = $this->model->where('senders_id',$user_id)->get();
        return $record;

    }

    public function getAllMessagebyAdmin()
    {
        $record = DB::table('messages')->where('visibility',"All")->get();
        return $record;

    }

    public function getAllMessagebyType($type)
    {
        $record = DB::table('messages')->whereIn('visibility',$type)->get();
        return $record;
    }


}
