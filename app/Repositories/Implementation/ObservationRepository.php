<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ObservationRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\Observation';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function listobservation($visibility,$arrondissements_id,$alerte)
    {


        $record = DB::table('observations')
                        ->whereIn('visibilities_id',$visibility)
                        ->whereIn('motifs_id',['1','2'])
                        ->whereNotIn('observations.id',$alerte)
                        ->join('engin_types','observations.engin_types_id','=','engin_types.id')
                        ->join('motifs','observations.motifs_id','=','motifs.id')
                        ->join('agents','observations.agents_id','=','agents.id')
                        ->join('users','agents.id','=','users.user_id')
                        ->where('agents.arrondissements_id',$arrondissements_id)
                        ->select('observations.id','observations.plaque','observations.couleur','observations.permis','observations.conducteur_nom','observations.conducteur_prenom','engin_types.description as engin','motifs.description as motif','users.name','users.firstname')
                        ->get();
        return $record;




    }

    public function listobservationfrontiere($visibility,$alerte)
    {

        $record = DB::table('observations')
                        ->whereIn('visibilities_id',$visibility)
                        ->whereIn('motifs_id',['3'])
                        ->whereNotIn('observations.id',$alerte)
                        ->join('engin_types','observations.engin_types_id','=','engin_types.id')
                        ->join('motifs','observations.motifs_id','=','motifs.id')
                        ->join('agents','observations.agents_id','=','agents.id')
                        ->join('users','agents.id','=','users.user_id')
                        ->join('countries','observations.countriesOut_id','=','countries.id')
                        ->select('observations.id','observations.plaque','observations.sejour','observations.couleur','observations.permis','observations.conducteur_nom','observations.conducteur_prenom','engin_types.description as engin','motifs.description as motif','users.name','users.firstname','observations.created_at','countries.nicename')
                        ->get();
        return $record;

    }
//->whereIn('observations.visibilities_id',['2','3'])
                        //->where('alertes.statut',0)
    public function findPlaqueAlerte($name,$type)
    {
        if($type == 'Police')
        {
            $record = DB::table('alertes')
                        ->join('observations','alertes.observations_id','=','observations.id')
                        ->where('observations.plaque',$name)
                        ->whereIn('observations.visibilities_id',['2','3'])
                        ->join('engin_types','observations.engin_types_id','=','engin_types.id')
                        ->join('motifs','observations.motifs_id','=','motifs.id')
                        ->select('alertes.id','alertes.statut','alertes.updated_at','observations.id  as observation_id','observations.visibilities_id','observations.countriesOut_id','observations.plaque','observations.created_at as debut','observations.couleur','observations.sejour','observations.finsejour','observations.permis','observations.conducteur_nom','observations.conducteur_prenom','engin_types.description as engin','motifs.description as motif')
                        ->get();

            return $record;

        }

        if($type == 'Gendarmerie')
        {
            $record = DB::table('alertes')
                        ->join('observations','alertes.observations_id','=','observations.id')
                        ->where('observations.plaque',$name)
                        ->whereIn('observations.visibilities_id',['1','3'])
                        ->join('engin_types','observations.engin_types_id','=','engin_types.id')
                        ->join('motifs','observations.motifs_id','=','motifs.id')
                        ->select('alertes.id','alertes.statut','alertes.updated_at','observations.id  as observation_id','observations.visibilities_id','observations.finsejour','observations.countriesOut_id','observations.plaque','observations.created_at as debut','observations.couleur','observations.sejour','observations.permis','observations.conducteur_nom','observations.conducteur_prenom','engin_types.description as engin','motifs.description as motif')
                        ->first();

            return $record;

        }

    }
//->whereIn('visibilities_id',['2','3'])
    public function findPlaqueObservation($name,$type)
    {



        if($type == 'Police')
        {
            $record = DB::table('observations')
                        ->where('plaque',$name)
                        ->whereIn('observations.visibilities_id',['2','3'])
                        ->join('engin_types','observations.engin_types_id','=','engin_types.id')
                        ->join('motifs','observations.motifs_id','=','motifs.id')
                        ->select('observations.id','observations.plaque','observations.sejour','observations.finsejour','observations.couleur','observations.permis','observations.conducteur_nom','observations.countriesOut_id','observations.conducteur_prenom','engin_types.description as engin','motifs.description as motif','observations.created_at as debut')
                        ->get();
            return $record;

        }

        if($type == 'Gendarmerie')
        {
            $record = DB::table('observations')
                        ->where('observations.plaque',$name)
                        ->whereIn('observations.visibilities_id',['1','3'])
                        ->join('engin_types','observations.engin_types_id','=','engin_types.id')
                        ->join('motifs','observations.motifs_id','=','motifs.id')
                        ->select('observations.id','observations.plaque','observations.finsejour','observations.sejour','observations.couleur','observations.permis','observations.conducteur_nom','observations.countriesOut_id','observations.conducteur_prenom','engin_types.description as engin','motifs.description as motif','observations.created_at as debut')
                        ->first();
            return $record;

        }

    }


}
