<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (Schema::hasTable('countries')) {
            Schema::table('countries',function(Blueprint $table){
                 if (!Schema::hasColumn('countries', 'iso')) {
                     $table->string('iso',150);
                 }
                 if (!Schema::hasColumn('countries', 'name')) {
                     $table->string('name',150);
                 }
                 if (!Schema::hasColumn('countries', 'nicename')) {
                     $table->string('nicename',150);
                 }
                 if (!Schema::hasColumn('countries', 'iso3')) {
                    $table->string('iso3',150);
                }
                if (!Schema::hasColumn('countries', 'numCode')) {
                    $table->string('numCode',150);
                }
                if (!Schema::hasColumn('countries', 'phoneCode')) {
                    $table->string('phoneCode',150);
                }



            });
        }

        if (!Schema::hasTable('countries')) {
            Schema::create('countries', function (Blueprint $table) {
                $table->id();
                $table->string('iso',150);
                $table->string('name',150);
                $table->string('nicename',150);
                $table->string('iso3',150);
                $table->string('numCode',150);
                $table->string('phoneCode',150);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
