<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnginTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('engin_types')) {
            Schema::table('engin_types',function(Blueprint $table){
                 if (!Schema::hasColumn('engin_types', 'description')) {
                     $table->string('description',150);
                 }
            });
        }

        if (!Schema::hasTable('engin_types')) {
            Schema::create('engin_types', function (Blueprint $table) {
                $table->id();
                $table->string('description',150);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('engin_types');
    }
}
