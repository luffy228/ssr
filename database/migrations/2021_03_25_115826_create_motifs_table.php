<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('motifs')) {
            Schema::table('motifs',function(Blueprint $table){
                 if (!Schema::hasColumn('motifs', 'description')) {
                     $table->string('description',150);
                 }
            });
        }

        if (!Schema::hasTable('motifs')) {
            Schema::create('motifs', function (Blueprint $table) {
                $table->id();
                $table->string('description',150);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motifs');
    }
}
