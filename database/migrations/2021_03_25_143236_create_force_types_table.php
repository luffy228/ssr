<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (Schema::hasTable('force_types')) {
            Schema::table('force_types',function(Blueprint $table){
                 if (!Schema::hasColumn('force_types', 'description')) {
                     $table->string('description',150);
                 }

                 if (!Schema::hasColumn('force_types', 'secteurType')) {
                    $table->string('secteurType',150);
                }
            });
        }

        if (!Schema::hasTable('force_types')) {
            Schema::create('force_types', function (Blueprint $table) {
                $table->id();
                $table->string('description',150);
                $table->string('secteurType',150);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('force_types');
    }
}
