<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (Schema::hasTable('visibilities')) {
            Schema::table('visibilities',function(Blueprint $table){
                 if (!Schema::hasColumn('visibilities', 'description')) {
                     $table->string('description',150);
                 }
            });
        }

        if (!Schema::hasTable('visibilities')) {
            Schema::create('visibilities', function (Blueprint $table) {
                $table->id();
                $table->string('description',150);
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visibilities');
    }
}
