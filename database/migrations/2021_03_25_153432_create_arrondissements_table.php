<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArrondissementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (Schema::hasTable('arrondissements')) {
            Schema::table('arrondissements',function(Blueprint $table){
                 if (!Schema::hasColumn('arrondissements', 'telephone')) {
                     $table->string('telephone',150);
                 }

                 if (!Schema::hasColumn('arrondissements', 'numero')) {
                    $table->string('numero',150);
                }

                if (!Schema::hasColumn('arrondissements', 'force_types_id')) {
                    $table->unsignedBigInteger('force_types_id');
                    $table->foreign('force_types_id')->references('id')->on('force_types');
                }
            });
        }

        if (!Schema::hasTable('arrondissements')) {
            Schema::create('arrondissements', function (Blueprint $table) {
                $table->id();
                $table->string('telephone',150);
                $table->string('numero',150);
                $table->unsignedBigInteger('force_types_id');
                $table->foreign('force_types_id')->references('id')->on('force_types');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arrondissements');
    }
}
