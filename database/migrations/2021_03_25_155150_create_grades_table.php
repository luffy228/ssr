<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('grades')) {
            Schema::table('grades',function(Blueprint $table){
                 if (!Schema::hasColumn('grades', 'description')) {
                     $table->string('description',150);
                 }

                 if (!Schema::hasColumn('grades', 'force_types_id')) {
                    $table->unsignedBigInteger('force_types_id');
                    $table->foreign('force_types_id')->references('id')->on('force_types');
                }
            });
        }

        if (!Schema::hasTable('grades')) {
            Schema::create('grades', function (Blueprint $table) {
                $table->id();
                $table->string('description',150);
                $table->unsignedBigInteger('force_types_id');
                $table->foreign('force_types_id')->references('id')->on('force_types');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grades');
    }
}
