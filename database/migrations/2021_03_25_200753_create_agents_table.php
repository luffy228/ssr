<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (Schema::hasTable('agents')) {
            Schema::table('agents',function(Blueprint $table){
                 if (!Schema::hasColumn('agents', 'longitude')) {
                     $table->double('longitude',150)->nullable();
                  }
                  if (!Schema::hasColumn('agents', 'latitude')) {
                    $table->double('latitude',150)->nullable();
                 }
                 if (!Schema::hasColumn('agents', 'grades_id')) {
                    $table->unsignedBigInteger('grades_id')->nullable();
                    $table->foreign('grades_id')->references('id')->on('grades');
                 }
                 if (!Schema::hasColumn('agents', 'latitude')) {
                    $table->unsignedBigInteger('arrondissements_id');
                    $table->foreign('arrondissements_id')->references('id')->on('arrondissements');
                 }
            });
        }

        if (!Schema::hasTable('agents')) {
            Schema::create('agents', function (Blueprint $table) {
                $table->id();
                $table->double('longitude',150)->nullable();
                $table->double('latitude',150)->nullable();
                $table->unsignedBigInteger('grades_id')->nullable();
                $table->foreign('grades_id')->references('id')->on('grades');

                $table->unsignedBigInteger('arrondissements_id');
                $table->foreign('arrondissements_id')->references('id')->on('arrondissements');
                $table->timestamps();
            });
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
