<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (Schema::hasTable('observations')) {
            Schema::table('observations',function(Blueprint $table){
                 if (!Schema::hasColumn('observations', 'plaque')) {
                     $table->string('plaque',150);
                  }
                  if (!Schema::hasColumn('observations', 'couleur')) {
                    $table->string('couleur',150)->nullable();
                 }

                 if (!Schema::hasColumn('observations', 'permis')) {
                    $table->string('permis',150)->nullable();
                 }

                 if (!Schema::hasColumn('observations', 'sejour')) {
                    $table->integer('sejour')->nullable();
                 }

                 if (!Schema::hasColumn('observations', 'conducteur_nom')) {
                    $table->string('conducteur_nom',150)->nullable();
                 }

                 if (!Schema::hasColumn('observations', 'conducteur_prenom')) {
                    $table->string('conducteur_prenom',150)->nullable();
                 }


                 if (!Schema::hasColumn('observations', 'engin_types_id')) {
                    $table->unsignedBigInteger('engin_types_id');
                    $table->foreign('engin_types_id')->references('id')->on('engin_types');
                 }
                 if (!Schema::hasColumn('observations', 'motifs_id')) {
                    $table->unsignedBigInteger('motifs_id');
                    $table->foreign('motifs_id')->references('id')->on('motifs');

                 }
                 if (!Schema::hasColumn('observations', 'countriesIn_id')) {
                    $table->unsignedBigInteger('countriesIn_id');
                    $table->foreign('countriesIn_id')->references('id')->on('countries');
                 }

                 if (!Schema::hasColumn('observations', 'countriesOut_id')) {
                    $table->unsignedBigInteger('countriesOut_id')->nullable();
                    $table->foreign('countriesOut_id')->references('id')->on('countries');
                 }

                 if (!Schema::hasColumn('observations', 'visibilities_id')) {
                    $table->unsignedBigInteger('visibilities_id');
                    $table->foreign('visibilities_id')->references('id')->on('visibilities');
                 }

                 if (!Schema::hasColumn('observations', 'agents_id')) {
                    $table->unsignedBigInteger('agents_id');
                    $table->foreign('agents_id')->references('id')->on('agents');
                 }

                 if (!Schema::hasColumn('observations', 'finsejour')) {
                    $table->date('finsejour')->nullable();
                 }
            });
        }

        if (!Schema::hasTable('observations')) {
            Schema::create('observations', function (Blueprint $table) {
                $table->id();
                $table->string('plaque',150);
                $table->string('couleur',150)->nullable();
                $table->string('permis',150)->nullable();
                $table->integer('sejour')->nullable();
                $table->string('conducteur_nom',150)->nullable();
                $table->string('conducteur_prenom',150)->nullable();
                $table->unsignedBigInteger('engin_types_id');
                $table->foreign('engin_types_id')->references('id')->on('engin_types');
                $table->unsignedBigInteger('motifs_id');
                $table->foreign('motifs_id')->references('id')->on('motifs');
                $table->unsignedBigInteger('countriesIn_id');
                $table->foreign('countriesIn_id')->references('id')->on('countries');
                $table->unsignedBigInteger('countriesOut_id')->nullable();
                $table->foreign('countriesOut_id')->references('id')->on('countries');
                $table->unsignedBigInteger('visibilities_id');
                $table->foreign('visibilities_id')->references('id')->on('visibilities');
                $table->unsignedBigInteger('agents_id');
                $table->foreign('agents_id')->references('id')->on('agents');
                $table->date('finsejour')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observations');
    }
}
