<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('alertes')) {
            Schema::table('alertes',function(Blueprint $table){
                 if (!Schema::hasColumn('alertes', 'statut')) {
                        $table->integer('statut');
                  }
                  if (!Schema::hasColumn('alertes', 'agents_id')) {
                    $table->unsignedBigInteger('agents_id');
                    $table->foreign('agents_id')->references('id')->on('agents');
                 }

                 if (!Schema::hasColumn('alertes', 'observations_id')) {
                    $table->unsignedBigInteger('observations_id');
                    $table->foreign('observations_id')->references('id')->on('observations');
                 }
            });
        }

        if (!Schema::hasTable('alertes')) {
            Schema::create('alertes', function (Blueprint $table) {
                $table->id();
                $table->integer('statut');
                $table->unsignedBigInteger('agents_id');
                $table->foreign('agents_id')->references('id')->on('agents');
                $table->unsignedBigInteger('observations_id');
                $table->foreign('observations_id')->references('id')->on('observations');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alertes');
    }
}
