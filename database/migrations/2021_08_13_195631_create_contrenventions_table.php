<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContrenventionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('contrenventions')) {
            Schema::table('contrenventions',function(Blueprint $table){
                 if (!Schema::hasColumn('contrenventions', 'plaque')) {
                     $table->string('plaque',150);
                  }

                 if (!Schema::hasColumn('contrenventions', 'agents_id')) {
                    $table->unsignedBigInteger('agents_id');
                    $table->foreign('agents_id')->references('id')->on('agents');
                 }

                 if (!Schema::hasColumn('contrenventions', 'arrondissements_id')) {
                    $table->unsignedBigInteger('arrondissements_id');
                    $table->foreign('arrondissements_id')->references('id')->on('arrondissements');
                 }

                 if (!Schema::hasColumn('contrenventions', 'statut')) {
                    $table->string('statut',150)->default("Valider");
                 }
            });
        }

        if (!Schema::hasTable('contrenventions')) {
            Schema::create('contrenventions', function (Blueprint $table) {
                $table->id();
                $table->string('plaque',150);
                $table->unsignedBigInteger('agents_id');
                $table->foreign('agents_id')->references('id')->on('agents');
                $table->unsignedBigInteger('arrondissements_id');
                $table->foreign('arrondissements_id')->references('id')->on('arrondissements');
                $table->string('statut',150)->default("Valider");

                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrenventions');
    }
}
