<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('pieces')) {
            Schema::table('pieces',function(Blueprint $table){
                 if (!Schema::hasColumn('pieces', 'description')) {
                     $table->string('description',150);
                 }
            });
        }

        if (!Schema::hasTable('pieces')) {
            Schema::create('pieces', function (Blueprint $table) {
                $table->id();
                $table->string('description',150);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pieces');
    }
}
