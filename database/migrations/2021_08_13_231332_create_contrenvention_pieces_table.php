<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContrenventionPiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (Schema::hasTable('contrenvention_pieces')) {
            Schema::table('contrenvention_pieces',function(Blueprint $table){

                 if (!Schema::hasColumn('contrenvention_pieces', 'contrenventions_id')) {
                    $table->unsignedBigInteger('contrenventions_id');
                    $table->foreign('contrenventions_id')->references('id')->on('contrenventions');
                 }

                 if (!Schema::hasColumn('contrenvention_pieces', 'pieces_id')) {
                    $table->unsignedBigInteger('pieces_id');
                    $table->foreign('pieces_id')->references('id')->on('pieces');
                 }


            });
        }

        if (!Schema::hasTable('contrenvention_pieces')) {
            Schema::create('contrenvention_pieces', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('contrenventions_id');
                $table->foreign('contrenventions_id')->references('id')->on('contrenventions');
                $table->unsignedBigInteger('pieces_id');
                $table->foreign('pieces_id')->references('id')->on('pieces');
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrenvention_pieces');
    }
}
