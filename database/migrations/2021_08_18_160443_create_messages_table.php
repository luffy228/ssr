<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('messages')) {
            Schema::table('messages',function(Blueprint $table){
                 if (!Schema::hasColumn('messages', 'objet')) {
                    $table->string('objet',150);

                  }

                 if (!Schema::hasColumn('messages', 'message')) {
                    $table->string('message',150);
                 }

                 if (!Schema::hasColumn('messages', 'senders_id')) {
                    $table->unsignedBigInteger('senders_id');
                    $table->foreign('senders_id')->references('id')->on('users');

                 }

                 if (!Schema::hasColumn('messages', 'read')) {
                    $table->string('read',150)->default("0");

                 }

                 if (!Schema::hasColumn('messages', 'visibility')) {
                    $table->string('visibility',150);
                 }
            });
        }

        if (!Schema::hasTable('messages')) {
            Schema::create('messages', function (Blueprint $table) {
                $table->id();
                $table->string('objet',150);
                $table->string('message',150);
                $table->unsignedBigInteger('senders_id');
                $table->foreign('senders_id')->references('id')->on('users');
                $table->string('read',150)->default("0");
                $table->string('visibility',150);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
