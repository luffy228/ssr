<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ForceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('force_types')->insertOrIgnore([
            ['description' => 'Gendarmerie','secteurType'=>'Brigade'],
            ['description' => 'Police','secteurType'=>'Commissariat'],
        ]);
    }
}
