<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PieceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pieces')->insertOrIgnore([
            ['description' => 'PC'],
            ['description' => 'Carte Grise'],
            ['description' => 'VT'],
            ['description' => 'Assurances'],
            ['description' => 'Carte bleue Taxi'],
            ['description' => 'Laisser Passer'],
        ]);
    }
}
