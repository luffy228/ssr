@extends('layouts.app')

@section('contenu')
<div class="main-panel">
    <div class="content content-full">
        <div class="page-wrapper has-sidebar">
            <div class="page-inner page-inner-fill">
                <div class="conversations">
                    <div class="message-header">
                        <div class="message-title">
                            <a class="btn btn-light" href="{{url()->previous()}}">
                                <i class="fa fa-flip-horizontal fa-share"></i>
                            </a>
                            <div class="user ml-2">
                                <div class="avatar">
                                    <img src="{{ asset('asset/img/user.png') }}" alt="Utilisateur" class="avatar-img rounded-circle border border-white">
                                </div>
                                <div class="info-user ml-2">
                                    @if (Auth::user()->type == "Admin")
                                        <span class="name">A tous les agents</span>
                                    @endif

                                    @if (Auth::user()->type == "Police")
                                        <span class="name">A tous les policiers</span>
                                    @endif

                                    @if (Auth::user()->type == "Gendarmerie")
                                        <span class="name">A tous les gendarmes</span>
                                    @endif

                                </div>
                            </div>
                            <div class="ml-auto">
                                <button class="btn btn-light page-sidebar-toggler d-xl-none">
                                    <i class="fa fa-angle-double-left"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="conversations-body">
                        <div class="conversations-content bg-white">
                            @foreach ($message as $message)
                            <div class="message-content-wrapper">
                                <div class="message message-in">
                                    <div class="avatar avatar-sm">
                                        <img src="{{ asset('asset/img/user.png') }}" alt="Utilisateur" class="avatar-img rounded-circle border border-white">
                                    </div>
                                    <div class="message-body">
                                        <div class="message-content">
                                            <div class="name">{{Auth::user()->type}}</div>
                                            <div class="content">{{$message["message"]}}</div>
                                        </div>
                                        <div class="date">{{$message["created_at"]}}</div>
                                    </div>
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
            <div class="page-sidebar">
                <header class="sidebar-header d-xl-none">
                    <a class="back" href="{{url()->previous()}}">
                        <i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Back
                    </a>
                </header>
                <div class="page-sidebar-section pt-3 pb-3">
                    <div class="text-center">
                        <div class="avatar avatar-xxl mb-3">
                            <img src="{{ asset('asset/img/user.png') }}" alt="Utilisateur" class="avatar-img rounded-circle border border-white">
                        </div>
                        @if (Auth::user()->type == "Admin")
                        <h4 class="fw-bold">A tous les agents</h4>
                        @endif

                        @if (Auth::user()->type == "Police")
                            <h4 class="fw-bold">A tous les policiers</h4>
                        @endif

                        @if (Auth::user()->type == "Gendarmerie")
                            <span class="name"></span>
                            <h4 class="fw-bold">A tous les gendarmes</h4>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
