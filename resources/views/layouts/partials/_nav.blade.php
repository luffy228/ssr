<div class="sidebar">

    <div class="sidebar-background"></div>
    <div class="sidebar-wrapper scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-1">
                    <img src="{{ asset('asset/img/user.png') }}" alt="utilisateur" class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            {{Auth::user()->name}}

                            <span class="user-level">
                                {{Auth::user()->getRoleNames()[0]}} </span>
                            <span class="user-level">{{Auth::user()->type}} </span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="{{ route('profile') }}">
                                    <span class="link-collapse">Mon Profile</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav">
                <li class="nav-item active">
                    <a href="{{ route('home') }}">
                        <i class="fas fa-home"></i>
                        <p>Acceuil</p>
                    </a>
                </li>
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Liens</h4>
                </li>

                @hasrole("SuperAdministrateur")
                    <li class="nav-item">
                        <a  href="{{ route('listeadmin',["type"=>"police"]) }}">
                            <i class="fas   fa-user-plus"></i>
                            <p>Administrateurs Police</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a  href="{{ route('listeadmin',["type"=>"gendarmerie"]) }}">
                            <i class="fas   fa-user-plus"></i>
                            <p>Administrateurs Gendarmerie</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a  href="{{ route('listemessage')}}">
                            <i class="fas fa-inbox"></i>
                            <p>Messagerie</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a  href="{{ route('logout') }}">
                            <i class="fas fa-eject"></i>
                            <p>Deconnexion</p>
                        </a>
                    </li>

                @endhasrole

            @hasrole("Administrateur")
                    @if (Auth::user()->type == "Police")
                        <li class="nav-item">
                            <a  href="{{ route('listesousadmin',["type"=>"police"]) }}">
                                <i class="fas   fa-user-plus"></i>
                                <p>Sous administrateurs Police</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listearrondissement',["type"=>"police"]) }}">
                                <i class="fas fa-archive"></i>
                                <p>Poste Police</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listemessage')}}">
                                <i class="fas fa-inbox"></i>
                                <p>Messagerie</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('logout') }}">
                                <i class="fas fa-eject"></i>
                                <p>Deconnexion</p>
                            </a>
                        </li>
                    @endif

                    @if (Auth::user()->type == "Gendarmerie")

                        <li class="nav-item">
                            <a  href="{{ route('listesousadmin',["type"=>"gendarmerie"]) }}">
                                <i class="fas   fa-user-plus"></i>
                                <p>Sous administrateurs Gendarmerie</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listearrondissement',["type"=>"gendarmerie"]) }}">
                                <i class="fas fa-archive"></i>
                                <p>Poste Gendarmerie</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listemessage')}}">
                                <i class="fas fa-inbox"></i>
                                <p>Messagerie</p>
                            </a>
                        </li>



                        <li class="nav-item">
                            <a  href="{{ route('logout') }}">
                                <i class="fas fa-eject"></i>
                                <p>Deconnexion</p>
                            </a>
                        </li>
                    @endif
            @endhasrole

            @hasrole("SousAdministrateur")
                    @if (Auth::user()->type == "Police")

                        <li class="nav-item">
                            <a  href="{{ route('listecontroleur',["type"=>"police"]) }}">
                                <i class="fas   fa-user-plus"></i>
                                <p>Controlleurs Police</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('maps',["type"=>"police"]) }}">
                                <i class="fas fa-map"></i>
                                <p>Localisation des agents</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listeAlerte',["type"=>"police"]) }}">
                                <i class="fas fa-key"></i>
                                <p>Alertes non resolus</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listeAlerteResolue',["type"=>"police"]) }}">
                                <i class="
                                fas fa-lock"></i>
                                <p>Alertes resolus</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listeobservation',["type"=>"police"]) }}">
                                <i class="fas fa-info"></i>
                                <p>Observation</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listeobservationfrontiere') }}">
                                <i class="fas fa-route"></i>
                                <p>Observation frontiere</p>
                            </a>
                        </li>



                        <li class="nav-item">
                            <a  href="{{ route('logout') }}">
                                <i class="fas fa-eject"></i>
                                <p>Deconnexion</p>
                            </a>
                        </li>

                    @endif

                    @if (Auth::user()->type == "Gendarmerie")

                        <li class="nav-item">
                            <a  href="{{ route('listecontroleur',["type"=>"gendarmerie"]) }}">
                                <i class="fas   fa-user-plus"></i>
                                <p>Controlleurs Gendarmerie</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('maps',["type"=>"gendarmerie"]) }}">
                                <i class="fas fa-map"></i>
                                <p>Localisation des agents</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listeAlerte',["type"=>"gendarmerie"]) }}">
                                <i class="fas fa-key"></i>
                                <p>Alertes non resolus</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listeAlerteResolue',["type"=>"gendarmerie"]) }}">
                                <i class="fas fa-lock"></i>
                                <p>Alertes resolus</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a  href="{{ route('listeobservation',["type"=>"gendarmerie"]) }}">
                                <i class="fas fa-info"></i>
                                <p>Observation</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a  href="{{ route('logout') }}">
                                <i class="fas fa-eject"></i>
                                <p>Deconnexion</p>
                            </a>
                        </li>
                    @endif
            @endhasrole
            </ul>
        </div>
    </div>
</div>
