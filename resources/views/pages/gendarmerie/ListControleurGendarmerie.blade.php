@extends('layouts.app')

@section('contenu')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Agents</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('home') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('listecontroleur',["type"=>"gendarmerie"]) }}">Liste des agents de terrain de la gendarmerie</a>
                        </li>

                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <h4 class="card-title">Ajouter un controlleur</h4>
                                        @if (Auth::user()->user_id != null)
                                            <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#exampleModal" data-whatever="Gendarmerie" data-user_id={{Auth::user()->user_id}}>
                                                <i class="fa fa-plus"></i>
                                                Ajouter
                                            </button>
                                        @else
                                            <em><strong>(Veuillez lui affecter un poste d'abord)</em>
                                        @endif

                                </div>
                            </div>
                            <div class="card-body">
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Ajouter un controlleur</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <form  action="{{ route('addcontroleur') }}" method="post">
                                                @csrf
                                            <div class="modal-body">


                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control" id="recipient-name" name="type" readonly >
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name" class="col-form-label">Nom:</label>
                                                        <input type="text" class="form-control" name="name" required>

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="firstname" class="col-form-label">Prenom:</label>
                                                        <input type="text" class="form-control" name="firstname" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="message-text" class="col-form-label">Email:</label>
                                                        <input type="email" class="form-control" name="email" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="message-text" class="col-form-label">Telephone:</label>
                                                        <input type="text" class="form-control" name="telephone" required minlength="8" maxlength="8">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="message-text" class="col-form-label">Nom d'utilisateur:</label>
                                                        <input type="text" class="form-control" name="username" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="message-text" class="col-form-label">Mot de passe:</label>
                                                        <input type="text" class="form-control" name="password" required minlength="8">
                                                    </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn  btn-primary">Ajouter</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>


                                <div class="table-responsive">
                                    <table id="add-row" class="display table table-striped table-hover" >
                                        <thead>
                                            <tr>
                                                <th>Nom</th>
                                                <th>Prenom</th>
                                                <th>Email</th>
                                                <th>Telephone</th>
                                                <th>Position</th>
                                                <th>Nombre observation</th>
                                                <th style="width: 10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Nom</th>
                                                <th>Prenom</th>
                                                <th>Email</th>
                                                <th>Telephone</th>
                                                <th>Position</th>
                                                <th>Nombre observation</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach ($gendarme as $gendarme)
                                            <tr>
                                                <td>{{$gendarme->name}}</td>
                                                <td>{{$gendarme->firstname}}</td>
                                                <td>{{$gendarme->email}}</td>
                                                <td>{{$gendarme->telephone}}</td>
                                                <td><a href="https://www.google.com/maps/search/?api=1&query={{$gendarme->latitude}},{{$gendarme->longitude}}" target="_blank"> Voir Position</a> </td>
                                                <?php
                                                    $countobservation = count(
                                                                             DB::table('observations')
                                                                                ->where('agents_id',$gendarme->user_id)
                                                                                ->get()
                                                    );
                                                ?>
                                                <td>{{$countobservation}}</td>
                                                <td>
                                                    <div class="form-button-action">
                                                        <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Supprimer">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </td>

                                            </tr>

                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




@section('js_special')

<!-- Page level custom scripts -->

<script>
     $('#exampleModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var user_id = button.data('user_id')
        var modal = $(this)

        modal.find('.modal-title').text('Ajouter un controlleur')
        modal.find('.modal-body #recipient-name').val(recipient)
        modal.find('.modal-body #user_id').val(user_id)
    })
</script>
<script >
    $(document).ready(function() {

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function() {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
                ]);
            $('#addRowModal').modal('hide');

        });
    });
</script>


@endsection









