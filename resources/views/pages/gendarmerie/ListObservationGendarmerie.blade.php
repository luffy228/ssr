@extends('layouts.app')

@section('contenu')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Observations
                        <span class="badge badge-count">{{count($observation)}}</span>
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('home') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('listeobservation',["type"=>"gendarmerie"]) }}">Liste des observations</a>
                        </li>

                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <!-- Modal -->
                                <div class="modal fade" id="affectation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <form  action="{{ route('addAlerte') }}" method="post">
                                                @csrf
                                            <div class="modal-body">
                                                    <input type="hidden" class="form-control" id="recipient-name" name="observation" readonly >
                                                    <input type="hidden" class="form-control" name="type" value="Gendarmerie" readonly >
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn  btn-primary">Ajouter</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>


                                <div class="table-responsive">
                                    <table id="add-row" class="display table table-striped table-hover" >
                                        <thead>
                                            <tr>
                                                <th>Agent</th>
                                                <th>Motif</th>
                                                <th>Plaque</th>
                                                <th>Couleur</th>
                                                <th>Engins</th>
                                                <th>Permis</th>
                                                <th>Conducteur</th>
                                                <th>Alerte</th>



                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Agent</th>
                                                <th>Motif</th>
                                                <th>Plaque</th>
                                                <th>Couleur</th>
                                                <th>Engins</th>
                                                <th>Permis</th>
                                                <th>Conducteur</th>
                                                <th>Alerte</th>

                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach ($observation as $observation)
                                                <tr>
                                                    <td>{{$observation->name}} {{$observation->firstname}}</td>
                                                    <td>{{$observation->motif}}</td>
                                                    <td>{{$observation->plaque}}</td>
                                                    <td>{{$observation->couleur}}</td>
                                                    <td>{{$observation->engin}}</td>
                                                    <td>{{$observation->permis}}</td>
                                                    <td>{{$observation->conducteur_nom}} {{$observation->conducteur_prenom}}</td>
                                                    <td><button type="button" class="btn  btn-primary " data-toggle="modal" data-target="#affectation" data-whatever={{$observation->id}}>Transformer en Alerte</button></td>
                                                </tr>

                                            @endforeach



                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection




@section('js_special')

<!-- Page level custom scripts -->

<script>
    $('#affectation').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var modal = $(this)
        modal.find('.modal-title').text('Transformation en Alerte')
        modal.find('.modal-body #recipient-name').val(recipient)
    })
</script>
<script >
    $(document).ready(function() {

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function() {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
                ]);
            $('#addRowModal').modal('hide');

        });
    });
</script>


@endsection









