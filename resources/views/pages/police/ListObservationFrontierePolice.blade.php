@extends('layouts.app')

@section('contenu')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Observations frontalieres
                        <span class="badge badge-count">{{count($observation)}}</span>
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('home') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('listeobservationfrontiere') }}">Liste des observations frontalieres</a>
                        </li>

                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <!-- Modal -->

                                <div class="table-responsive">
                                    <table id="add-row" class="display table table-striped table-hover" >
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Plaque</th>
                                                <th>Couleur</th>
                                                <th>Engins</th>
                                                <th>Permis</th>
                                                <th>Conducteur</th>
                                                <th>Sejour</th>
                                                <th>Provenance</th>



                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Date</th>
                                                <th>Plaque</th>
                                                <th>Couleur</th>
                                                <th>Engins</th>
                                                <th>Permis</th>
                                                <th>Conducteur</th>
                                                <th>Sejour</th>
                                                <th>Provenance</th>

                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach ($observation as $observation)
                                                <tr>
                                                    <td>{{$observation->created_at}}</td>
                                                    <td>{{$observation->plaque}}</td>
                                                    <td>{{$observation->couleur}}</td>
                                                    <td>{{$observation->engin}}</td>
                                                    <td>{{$observation->permis}}</td>
                                                    <td>{{$observation->conducteur_nom}} {{$observation->conducteur_prenom}}</td>
                                                    <td>{{$observation->sejour}}</td>
                                                    <td>{{$observation->nicename}}</td>
                                                </tr>

                                            @endforeach



                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection




@section('js_special')

<!-- Page level custom scripts -->
<script >
    $(document).ready(function() {

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function() {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
                ]);
            $('#addRowModal').modal('hide');

        });
    });
</script>


@endsection









