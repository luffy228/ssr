@extends('layouts.app')

@section('contenu')
<div class="main-panel">
    <div class="content content-full">
        <div class="page-navs bg-white">
            <ul class="nav nav-pills nav-secondary  nav-pills-no-bd nav-pills-icons justify-content-center" id="pills-tab-with-icon" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab-icon" data-toggle="pill" href="#pills-home-icon" role="tab" aria-controls="pills-home-icon" aria-selected="true">
                        Messages de diffussion
                    </a>
                </li>
            </ul>
        </div>
        <div class="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between">
                        <button type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-success d-none d-sm-inline-block">Nouveau message de diffussion</button>
                    </div>

                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Message de diffussion</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <form  action="{{ route('messagesend') }}" method="post">
                                    @csrf
                                <div class="modal-body">

                                        <div class="form-group">
                                            <label for="name" class="col-form-label">Objet:</label>
                                            <input type="text" class="form-control" name="objet" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="firstname" class="col-form-label">Message:</label>
                                            <textarea class="form-control" id="comment" name="message" rows="5">

                                            </textarea>
                                        </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn  btn-primary">Ajouter</button>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>







                    <!-- Cette section depend maintenant du type de l'utilisateur  -->
                    @if (Auth::user()->type == "Admin")
                        <section class="card mt-4 tab-pane fade show active" id="pills-home-icon" role="tabpanel">
                            <div class="list-group list-group-messages list-group-flush " >
                                <div class="list-group-item unread">
                                    <div class="list-group-item-figure">
                                        <a href="{{route('messagehistory')}}" class="user-avatar">
                                            <div class="avatar">
                                                <img src="{{ asset('asset/img/user.png') }}" alt="Utilisateur" class="avatar-img rounded-circle">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="list-group-item-body pl-3 pl-md-4">
                                        <div class="row">
                                            <div class="col-12 col-lg-10">
                                                <h4 class="list-group-item-title">
                                                    <a href="{{route('messagehistory')}}">Tous les agents</a>
                                                </h4>
                                                @if ($message == null)
                                                <p class="list-group-item-text text-truncate"> Aucun message </p>
                                                @endif
                                                @if ($message != null)
                                                <p class="list-group-item-text text-truncate"> {{$message["message"]}} </p>
                                                @endif

                                            </div>
                                            <div class="col-12 col-lg-2 text-lg-right">
                                                @if ($message == null)
                                                <p class="list-group-item-text text-truncate"></p>
                                                @endif
                                                @if ($message != null)
                                                <p class="list-group-item-text">{{$message["created_at"]}} </p>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    @endif

                    @if (Auth::user()->type == "Police")
                        <section class="card mt-4 tab-pane fade show active" id="pills-home-icon" role="tabpanel">
                            <div class="list-group list-group-messages list-group-flush " >
                                <div class="list-group-item unread">
                                    <div class="list-group-item-figure">
                                        <a href="{{route('messagehistory')}}" class="user-avatar">
                                            <div class="avatar">
                                                <img src="{{ asset('asset/img/user.png') }}" alt="Utilisateur" class="avatar-img rounded-circle">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="list-group-item-body pl-3 pl-md-4">
                                        <div class="row">
                                            <div class="col-12 col-lg-10">
                                                <h4 class="list-group-item-title">
                                                    <a href="{{route('messagehistory')}}">A tous les policier</a>
                                                </h4>
                                                @if ($message == null)
                                                <p class="list-group-item-text text-truncate"> Aucun message </p>
                                                @endif
                                                @if ($message != null)
                                                <p class="list-group-item-text text-truncate"> {{$message["message"]}} </p>
                                                @endif

                                            </div>
                                            <div class="col-12 col-lg-2 text-lg-right">
                                                @if ($message == null)
                                                <p class="list-group-item-text text-truncate"></p>
                                                @endif
                                                @if ($message != null)
                                                <p class="list-group-item-text">{{$message["created_at"]}} </p>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="list-group-item unread">
                                    <div class="list-group-item-figure">
                                        <a href="{{route('messagehistoryAdmin')}}" class="user-avatar">
                                            <div class="avatar">
                                                <img src="{{ asset('asset/img/user.png') }}" alt="Utilisateur" class="avatar-img rounded-circle">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="list-group-item-body pl-3 pl-md-4">
                                        <div class="row">
                                            <div class="col-12 col-lg-10">
                                                <h4 class="list-group-item-title">
                                                    <a href="{{route('messagehistoryAdmin')}}">Super Administrateur</a>
                                                </h4>
                                                @if ($adminMessage != null)
                                                <p class="list-group-item-text text-truncate"> {{$adminMessage->message}} </p>
                                                @endif

                                                @if ($adminMessage == null)
                                                <p class="list-group-item-text text-truncate"> Aucun message </p>
                                                @endif


                                            </div>
                                            <div class="col-12 col-lg-2 text-lg-right">

                                                @if ($adminMessage != null)
                                                    <p class="list-group-item-text">{{$adminMessage->created_at}} </p>
                                                @endif

                                                @if ($adminMessage == null)
                                                    <p class="list-group-item-text"></p>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    @endif

                    @if (Auth::user()->type == "Gendarmerie")
                        <section class="card mt-4 tab-pane fade show active" id="pills-home-icon" role="tabpanel">
                            <div class="list-group list-group-messages list-group-flush " >

                                <div class="list-group-item unread">
                                    <div class="list-group-item-figure">
                                        <a href="{{route('messagehistory')}}" class="user-avatar">
                                            <div class="avatar">
                                                <img src="{{ asset('asset/img/user.png') }}" alt="Utilisateur" class="avatar-img rounded-circle">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="list-group-item-body pl-3 pl-md-4">
                                        <div class="row">
                                            <div class="col-12 col-lg-10">
                                                <h4 class="list-group-item-title">
                                                    <a href="{{route('messagehistory')}}">Tous les gendarmes</a>
                                                </h4>
                                                @if ($message == null)
                                                <p class="list-group-item-text text-truncate"> Aucun message </p>
                                                @endif
                                                @if ($message != null)
                                                <p class="list-group-item-text text-truncate"> {{$message["message"]}} </p>
                                                @endif

                                            </div>
                                            <div class="col-12 col-lg-2 text-lg-right">
                                                @if ($message == null)
                                                <p class="list-group-item-text text-truncate"></p>
                                                @endif
                                                @if ($message != null)
                                                <p class="list-group-item-text">{{$message["created_at"]}} </p>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="list-group-item unread">
                                    <div class="list-group-item-figure">
                                        <a href="{{route('messagehistoryAdmin')}}" class="user-avatar">
                                            <div class="avatar">
                                                <img src="{{ asset('asset/img/user.png') }}" alt="Utilisateur" class="avatar-img rounded-circle">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="list-group-item-body pl-3 pl-md-4">
                                        <div class="row">
                                            <div class="col-12 col-lg-10">
                                                <h4 class="list-group-item-title">
                                                    <a href="{{route('messagehistoryAdmin')}}">Super Administrateur</a>
                                                </h4>
                                                @if ($adminMessage != null)
                                                <p class="list-group-item-text text-truncate"> {{$adminMessage->message}} </p>
                                                @endif

                                                @if ($adminMessage == null)
                                                <p class="list-group-item-text text-truncate"> Aucun message </p>
                                                @endif


                                            </div>
                                            <div class="col-12 col-lg-2 text-lg-right">

                                                @if ($adminMessage != null)
                                                    <p class="list-group-item-text">{{$adminMessage->created_at}} </p>
                                                @endif

                                                @if ($adminMessage == null)
                                                    <p class="list-group-item-text"></p>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>




@endsection




@section('js_special')

<!-- Page level custom scripts -->





@endsection









