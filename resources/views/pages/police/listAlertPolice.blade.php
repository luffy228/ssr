@extends('layouts.app')

@section('contenu')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Alerte non resolue
                    <span class="badge badge-count">{{count($observation)}}</span>

                </h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="{{ route('home') }}">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('listeAlerte',["type"=>"police"]) }}">Liste des alertes</a>
                    </li>

                </ul>
            </div>
<div class="col-md-12">
    <div class="row">

        @foreach ($observation as $observation)

            @if ($observation->motif != "Sejour")

                <div class="col-sm-3">
                    <div class="card">

                        @if ($observation->visibilities_id == 3)
                            <h5 class="text-center" style="color: red">(Partager entre la police et la gendarmerie)</h5>
                        @endif
                        <h4 class="text-center" style="color: red">Motif: {{$observation->motif}}</h4>
                        <h4 class="text-center" style="color: yellowgreen">{{$observation->engin}} de couleur  {{$observation->couleur}}</h4>
                        <h4 class="text-center" style="color: yellowgreen">Numero de plaque: {{$observation->plaque}}</h4>
                        <div class="card-body text-size">
                            <p>Conducteur:{{$observation->conducteur_nom}} {{$observation->conducteur_prenom}}</p>
                            <p>Numero de permis:{{$observation->permis}}</p>

                            <div class="row">
                                <button class="btn btn-danger has-ripple  ml-2" data-toggle="modal" data-target="#resolue" data-whatever={{$observation->id}}>Resolue<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
                                @if ($observation->visibilities_id != 3)
                                    <button class="btn btn-primary has-ripple  ml-2" data-toggle="modal" data-target="#affectation" data-whatever={{$observation->observation_id}}>Partager<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
                                @else

                                @endif

                            </div>
                        </div>
                    </div>
                </div>

            @endif

            @if ($observation->motif == "Sejour")

                <div class="col-sm-4">
                    <div class="card">
                        @if ($observation->visibilities_id == 3)
                            <h5 class="text-center" style="color: red">(Partager entre la police et la gendarmerie)</h5>
                        @endif
                        <h4 class="text-center" style="color: red">Motif: {{$observation->motif}} Expire</h4>
                        <h4 class="text-center" style="color: green">{{$observation->engin}} {{$observation->couleur}} : {{$observation->plaque}}</h4>
                        <div class="card-body text-size">
                            <p>Conducteur:{{$observation->conducteur_nom}} {{$observation->conducteur_prenom}}</p>
                            <p>Numero de permis:{{$observation->permis}}</p>
                            <?php

                                $pays = DB::table('countries')->where('id',$observation->countriesOut_id)->first();

                            ?>
                            <p>Provenance:{{$pays->nicename}}</p>

                            <div class="row">
                                <button class="btn btn-danger has-ripple  ml-2" data-toggle="modal" data-target="#resolue" data-whatever={{$observation->id}}>Resolue<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
                                @if ($observation->visibilities_id != 3)
                                <button class="btn btn-primary has-ripple  ml-2" data-toggle="modal" data-target="#affectation" data-whatever={{$observation->observation_id}}>Partager<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
                                @else

                                @endif

                            </div>
                        </div>
                    </div>
                </div>

            @endif



        @endforeach







    </div>
</div>
</div>
</div>
</div>



<div class="modal fade" id="affectation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Affecter un poste</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form  action="{{ route('changeVisibility') }}" method="post">
                @csrf
            <div class="modal-body">
                    <input type="hidden" class="form-control" id="recipient-name" name="observation" readonly >
                    <input type="hidden" class="form-control"  name="type" value="Police" readonly >



            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn  btn-primary">Ajouter</button>
            </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="resolue" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Etes vous sur?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form  action="{{ route('alerteResolue') }}" method="post">
                @csrf
            <div class="modal-body">
                    <input type="hidden" class="form-control" id="recipient-name" name="observation" readonly >



            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn  btn-primary">Oui</button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection


@section('js_special')
<script>


    $('#affectation').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var modal = $(this)
        modal.find('.modal-title').text('Partager')
        modal.find('.modal-body #recipient-name').val(recipient)
    })

    $('#resolue').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var modal = $(this)
        modal.find('.modal-title').text('Etes vous sur?')
        modal.find('.modal-body #recipient-name').val(recipient)
    })
</script>


@endsection









