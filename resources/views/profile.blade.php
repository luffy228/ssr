@extends('layouts.app')

@section('contenu')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Informations personnel</h4>
                            </div>
                            <div class="card-body">
                                <ul class="nav nav-pills nav-secondary  nav-pills-no-bd nav-pills-icons justify-content-center" id="pills-tab-with-icon" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-home-tab-icon" data-toggle="pill" href="#pills-home-icon" role="tab" aria-controls="pills-home-icon" aria-selected="true">
                                            <i class="flaticon-home"></i>
                                            Modifier information personnel
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab-icon" data-toggle="pill" href="#pills-profile-icon" role="tab" aria-controls="pills-profile-icon" aria-selected="false">
                                            <i class="flaticon-user-4"></i>
                                            Modifier mot de passe
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content mt-2 mb-3" id="pills-with-icon-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home-icon" role="tabpanel" aria-labelledby="pills-home-tab-icon">
                                        <form action="{{ route('updatecompte') }}" method="POST">
                                            @csrf
                                            <div class="row mt-3">
                                                <div class="col-md-4">
                                                    <div class="form-group form-group-default">
                                                        <label>Nom</label>
                                                        <input type="text" class="form-control" id="datepicker" name="name" value="{{Auth::user()->name}}" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-group-default">
                                                        <label>Prenom</label>
                                                        <input type="text" class="form-control" id="datepicker" name="firstname" value="{{Auth::user()->firstname}}" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-group-default">
                                                        <label>Phone</label>
                                                        <input type="text" class="form-control" value="{{Auth::user()->telephone}}" name="phone" placeholder="Phone" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-2">
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-default">
                                                        <label>Nom d'utilisateur</label>
                                                        <input type="text" class="form-control" id="datepicker" name="username" value="{{Auth::user()->username}}" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-default">
                                                        <label>Email</label>
                                                        <input type="email" class="form-control" id="datepicker" name="email" value="{{Auth::user()->email}}" placeholder="Birth Date" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-action mb-3">
                                                <button class="btn btn-primary btn-rounded btn-login">Modifier</button>
                                            </div>
                                        </form>
									</div>


                                    <div class="tab-pane fade" id="pills-profile-icon" role="tabpanel" aria-labelledby="pills-profile-tab-icon">
                                        <form action="{{ route('updatepassword') }}" method="POST">
                                            @csrf
                                            <div class="row mt-2">
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-default">
                                                        <label>Ancien mot de passe</label>
                                                        <input type="text" class="form-control" id="datepicker" name="lastpassword"  placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-default">
                                                        <label>Nouveau mot de passe</label>
                                                        <input type="text" class="form-control" id="datepicker" name="newpassword"  placeholder="" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-action mb-3">
                                                <button class="btn btn-primary btn-rounded btn-login">Modifier</button>
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-profile card-secondary">
                            <div class="card-header" style="background-color:white">
                                <div class="profile-picture">
                                    <div class="avatar avatar-xl">
                                        <img src="{{ asset('asset/img/user.png') }}" alt="..." class="avatar-img rounded-circle">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body" style="background-color: white">
                                <div class="user-profile text-center">
                                    <div class="name">{{Auth::user()->name}}</div>
                                    <div class="job">{{Auth::user()->type}}</div>
                                    <div class="desc">{{Auth::user()->getRoleNames()[0]}} </div>
                                    <div class="job">Email :{{Auth::user()->email}} / Telephone :{{Auth::user()->telephone}} </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
