@extends('layouts.app')

@section('contenu')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Resultat de la recherche</h4>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info bg-info-gradient">
                        <div class="card-header">
                            <div class="card-title" style="text-align: center"><strong> Plaque :   {{$data["plaque"]}} </strong>

                                @if ($data["statut"] == null)
                                    Rien a signaler
                                @endif
                                @if ($data["statut"] != null)
                                    ({{$data["statut"]}})/Engin :{{$data["engin"]}}
                                @endif
                                / contrenvention : {{$data["contrenvention"]}}  </div>
                        </div>
                        <div class="card-body pb-0">
                            @if ($data["statut"] != null)
                            <div class="row">
                                <div class="col-md-4"><h3>Conducteur :{{$data["conducteur"]}} </h3></div>
                                <div class="col-md-4"><h3>Permis : {{$data["permis"]}}</h3></div>
                                <div class="col-md-4"><h3>Couleur : {{$data["couleur"]}}</h3></div>

                            </div>
                            <div class="mb-4 mt-2">
                            </div>
                            <div class="row">
                                <div class="col-md-4"><h3>Motif : {{$data["motif"]}}</h3></div>
                                <div class="col-md-4"><h3>Alerte : {{$data["countAlerte"]}} </h3></div>
                                <div class="col-md-4"><h3>Contrenvention : {{$data["countContrenvention"]}}</h3></div>
                            </div>
                            <div class="mb-4 mt-2">
                            </div>
                                @if ($data["motif"] == "sejour" || $data["motif"] == "sejour expirer")
                                    <div class="row">
                                        <div class="col-md-4"><h3>Provenance : {{$data["pays"]}}</h3></div>
                                        <div class="col-md-4"><h3>Duree Sejour :{{$data["sejour"]}} jours</h3></div>
                                        <div class="col-md-4"><h3>Date d'entree : {{$data["entree"]}}</h3></div>
                                    </div>
                                @endif
                            @endif

                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-head-row">
                                <div class="card-title">Recapitulatif Alertes ou Observations </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (count($alertes) == 0)

                                @if (count($observation) == 0)
                                    <p><em>Aucune Observation ou alerte trouver</em></p>
                                @endif
                                @if (count($observation) != 0)

                                    @foreach ($observation as $observation)

                                        <div class="d-flex">
                                            <div class="avatar avatar-online">
                                                <img src="{{asset('asset/img/embleme.png')}}" width="80%">
                                            </div>
                                            <div class="flex-1 ml-3 pt-1">
                                                <h5 class="text-uppercase fw-bold mb-1">Conducteur : {{$observation->conducteur_nom}} {{$observation->conducteur_prenom}} <span class="text-warning pl-3">Observation</span></h5>
                                                <span class="text-muted">{{$observation->engin}} de couleur {{$observation->couleur}} a ete enregistrer pour le motif suivant : {{$observation->motif}}.
                                                    @if ($observation->motif == "Sejour")
                                                    <?php
                                                        if ($observation->countriesOut_id == null) {
                                                            $pays = "Non Renseigner";
                                                        }
                                                        if ($observation->countriesOut_id != null) {
                                                            $country = DB::table('countries')->where('id',$observation->countriesOut_id)->first();
                                                            $pays = $country->nicename;
                                                        }
                                                    ?>
                                                    Il est rentrer au Togo le {{$observation->debut}} en provenance du {{$pays}} pour un sejour de {{$observation->sejour}} jours.
                                                @endif

                                            </span>
                                            </div>
                                            <div class="float-right pt-1">
                                                <small class="text-muted">{{$observation->debut}}</small>
                                            </div>
                                        </div>
                                        <div class="separator-dashed"></div>
                                    @endforeach
                                @endif

                            @endif

                            @if (count($alertes) != 0)

                                    @foreach ($alertes as $alertes)

                                        <div class="d-flex">
                                            <div class="avatar avatar-online">
                                                <img src="{{asset('asset/img/embleme.png')}}" width="80%">
                                            </div>
                                            <div class="flex-1 ml-3 pt-1">
                                                <h5 class="text-uppercase fw-bold mb-1">Conducteur : {{$alertes->conducteur_nom}} {{$alertes->conducteur_prenom}}

                                                       @if ($alertes->statut ==0)
                                                        <span class="text-warning pl-3">Alerte </span> </h5>
                                                        <span class="text-muted">{{$alertes->engin}} de couleur {{$alertes->couleur}} est en etat d'alerte  pour le motif suivant : {{$alertes->motif}}.
                                                       @endif

                                                       @if ($alertes->statut ==1)
                                                        <span class="text-muted pl-3">Resolue</span></h5>
                                                        <span class="text-muted">{{$alertes->engin}} de couleur {{$alertes->couleur}} a ete arreter  pour le motif suivant : {{$alertes->motif}}.
                                                       @endif


                                                    @if ($alertes->motif == "Sejour")
                                                    <?php
                                                        if ($alertes->countriesOut_id == null) {
                                                            $pays = "Non Renseigner";
                                                        }
                                                        if ($alertes->countriesOut_id != null) {
                                                            $country = DB::table('countries')->where('id',$alertes->countriesOut_id)->first();
                                                            $pays = $country->nicename;
                                                        }
                                                    ?>
                                                    Il est rentrer au Togo le {{$alertes->debut}} en provenance du {{$pays}} pour un sejour de {{$alertes->sejour}} jours.
                                                @endif

                                            </span>
                                            </div>
                                            <div class="float-right pt-1">
                                                <small class="text-muted">{{$alertes->updated_at}}</small>
                                            </div>
                                        </div>
                                        <div class="separator-dashed"></div>
                                    @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-head-row">
                                <div class="card-title">Liste des contrenventions</div>
                            </div>
                        </div>
                        <div class="card-body">

                            @if (count($contrenvention) == 0)
                                    <p><em>Aucune contrenvention trouver</em></p>
                            @endif

                            @if (count($contrenvention) != 0)
                                @foreach ($contrenvention as $contrenvention)
                                    <?php $piece = DB::table('contrenvention_pieces')->where('contrenventions_id',$contrenvention->id)
                                                    ->join('pieces','pieces.id','=','contrenvention_pieces.pieces_id')
                                                    ->get() ?>
                                    <div class="d-flex">
                                        <div class="avatar avatar-online">
                                            <img src="{{asset('asset/img/embleme.png')}}" width="80%">
                                        </div>
                                        <div class="flex-1 ml-3 pt-1">
                                            <h5 class="text-uppercase fw-bold mb-1">Plaque : {{$contrenvention->plaque}}
                                                @if ($contrenvention->statut =="Valider")
                                                    <span class="text-warning pl-3">En cours </span> </h5>
                                                @endif

                                                @if ($contrenvention->statut =="Retirer")
                                                    <span class="text-muted pl-3">Regler le {{$contrenvention->updated_at}}</span></h5>
                                                @endif
                                                @if (count($piece) == 0)
                                                    <span class="text-muted">Aucune piece recuperer</span>
                                                @endif

                                                @if (count($piece) != 0)
                                                    <span class="text-muted"> Liste des pieces recuperer :
                                                        @foreach ( $piece as $piece)
                                                        {{$piece->description}},
                                                        @endforeach
                                                    </span>
                                                @endif




                                        </div>
                                        <div class="float-right pt-1">
                                            <small class="text-muted">{{$contrenvention->created_at}}</small>
                                        </div>
                                    </div>
                                    <div class="separator-dashed"></div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
