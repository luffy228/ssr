<?php

use App\Http\Controllers\api\ContrenventionController;
use App\Http\Controllers\api\MessageController;
use App\Http\Controllers\api\ObservationController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/v1')->group(function () {
    Route::post('/login',[UserController::class,'login']);
    Route::post('logout','UserController@logout');

});

// Pour seulement le role controlleur

Route::middleware(['auth:api','role:Controleur'])->prefix("/v1/controlleur")->group(function () {
    Route::post('/addObservation',[ObservationController::class,'add']);
    Route::post('/addContrenvention',[ContrenventionController::class,'add']);
    Route::post('/recherche',[ObservationController::class,'recherche']);
    Route::post('/listMessage',[MessageController::class,'message']);
});

