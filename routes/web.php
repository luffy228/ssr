<?php

use App\Http\Controllers\ArrondissementController;
use App\Http\Controllers\EnginTypeController;
use App\Http\Controllers\ForceTypeController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MotifController;
use App\Http\Controllers\ObservationController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\web\AlerteController;
use App\Http\Controllers\web\ArrondissementController as WebArrondissementController;
use App\Http\Controllers\web\GeneriqueController;
use App\Http\Controllers\web\MapsController;
use App\Http\Controllers\web\MesssagerieController;
use App\Http\Controllers\web\ObservationController as WebObservationController;
use App\Http\Controllers\web\RechercheController;
use App\Http\Controllers\web\UserController as WebUserController;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/admin',[HomeController::class,'adminPage'])->name('admin');
Route::get('/',[HomeController::class,'index'])->name('login');
Route::get('/logout',[HomeController::class,'logout'])->name('logout');




Route::middleware(['role:SuperAdministrateur|Administrateur|SousAdministrateur'])->prefix("/ssr")->group(function () {
    Route::get('/home',[GeneriqueController::class,'index'])->name('home');
    Route::get('/profile',[GeneriqueController::class,'profile'])->name('profile');
    Route::post('/updatecompte',[GeneriqueController::class,'updateCompte'])->name('updatecompte');
    Route::post('/updatepassword',[GeneriqueController::class,'updatePassword'])->name('updatepassword');
});
Route::middleware(['role:SuperAdministrateur'])->prefix("/superadmin")->group(function () {
    Route::get('/listAdmin/{type}', [WebUserController::class,'listAdmin'])->name('listeadmin');
    Route::post('/addAdmin',[WebUserController::class,'addAdministrateur'])->name('addadmin');
});

Route::middleware(['role:SuperAdministrateur|Administrateur'])->prefix("/superadmin")->group(function () {
    Route::get('/listemessage',[MesssagerieController::class,'messageAll'])->name('listemessage');
    Route::get('/messagehistory',[MesssagerieController::class,'messageHistory'])->name('messagehistory');
    Route::post('/message',[MesssagerieController::class,'messageSend'])->name('messagesend');
});

Route::middleware(['role:Administrateur'])->prefix("/admin")->group(function () {
    Route::get('/listesousadmin/{type}',[WebUserController::class,'SousAdminList'] )->name('listesousadmin');
    Route::post('/addSousAdmin',[WebUserController::class,'addSousCompte'])->name('addSousAdmin');
    Route::get('/listArrondissement/{type}',[WebArrondissementController::class,'listArrondissement'])->name('listearrondissement');
    Route::post('/addArrondissement',[WebArrondissementController::class,'add'])->name('addarrondissement');
    Route::post('/affecterSousAdmin',[WebUserController::class,'affecterSousAdmin'])->name('affecterSousAdmin');
    Route::get('/adminmessagehistory',[MesssagerieController::class,'messageHistoryAdmin'])->name('messagehistoryAdmin');
});

Route::middleware(['role:SousAdministrateur'])->prefix("/sousadmin")->group(function () {
    Route::get('/listecontroleur/{type}',[WebUserController::class,'listecontrolleur'])->name('listecontroleur');
    Route::post('/addControlleur',[WebUserController::class,'addControlleur'])->name('addcontroleur');
    Route::get('/listeAlerte/{type}',[AlerteController::class,'listeAlerte'])->name('listeAlerte');
    Route::get('/listeAlerteResolue/{type}',[AlerteController::class,'listeAlerteResolue'])->name('listeAlerteResolue');
    Route::post('/changeVisibility',[AlerteController::class,'changeVisibility'])->name('changeVisibility');
    Route::post('/alerteResolue',[AlerteController::class,'alerteResolue'])->name('alerteResolue');
    Route::post('/addAlerte',[AlerteController::class,'add'])->name('addAlerte');
    Route::get('/listeobservation/{type}',[WebObservationController::class,'ObservationListe'])->name('listeobservation');
    Route::get('/listeobservationfrontiere',[WebObservationController::class,'ObservationFrontiereListe'])->name('listeobservationfrontiere');
    Route::get('/maps/{type}',[MapsController::class,'maps'])->name('maps');
    Route::post('/rechercheplaque',[RechercheController::class,'recherche'])->name('recherche');
});


// Role Sous Administrateur Administrateur SousAdministrateur

// Administrateur


// Sous administrateur























//Auth::routes();


